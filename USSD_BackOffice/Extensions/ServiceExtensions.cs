﻿using Contracts;
using Entities;
using LoggerService;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Repository;
using Entities.Models;
using Entities.Helper;

namespace APH_Advans.Extensions
{
    public static class ServiceExtensions
    {
        public static void ConfigureCors(this IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    builder => builder.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader());
            });
        }
        public static void ConfigureIISIntegration(this IServiceCollection services)
        {
            services.Configure<IISOptions>(options =>
            {
            });
        }
        public static void ConfigureLoggerService(this IServiceCollection services)
        {
            services.AddSingleton<ILoggerManager, LoggerManager>();
        }
        public static void ConfigureSqlServerContext(this IServiceCollection services, IConfiguration config)
        {
            var connectionString = config["sqlServerConnection:connectionString"];
            services.AddDbContext<RepositoryContext>(o => o.UseSqlServer(connectionString));
        }
        public static void ConfigureRepositoryWrapper(this IServiceCollection services)
        {
            services.AddScoped<ISortHelper<POC_OWN_Owner>, SortHelper<POC_OWN_Owner>>();
            services.AddScoped<ISortHelper<POC_ACC_Account>, SortHelper<POC_ACC_Account>>();
            services.AddScoped<ISortHelper<APH_V_Monitoring_Partenaire>, SortHelper<APH_V_Monitoring_Partenaire>>();
            services.AddScoped<ISortHelper<APH_RMI_RapprochementManuelleIncohernce>, SortHelper<APH_RMI_RapprochementManuelleIncohernce>>();
            services.AddScoped<ISortHelper<APH_RMD_RapprochementManuelleDoublons>, SortHelper<APH_RMD_RapprochementManuelleDoublons>>();
            services.AddScoped<ISortHelper<APH_RDS_RapprochementDesSuspendus>, SortHelper<APH_RDS_RapprochementDesSuspendus>>();
            services.AddScoped<ISortHelper<APH_HDR_HistoriqueDesRapprochements>, SortHelper<APH_HDR_HistoriqueDesRapprochements>>();
            services.AddScoped<ISortHelper<APH_TDA_TranscoDonneesAmplitude>, SortHelper<APH_TDA_TranscoDonneesAmplitude>>();
            services.AddScoped<ISortHelper<APH_ISC_IncoherenceSuggestedCorrection>, SortHelper<APH_ISC_IncoherenceSuggestedCorrection>>();
            services.AddScoped<ISortHelper<amen_transaction_payee>, SortHelper<amen_transaction_payee>>();
            services.AddScoped<ISortHelper<attijeri_transaction_payee>, SortHelper<attijeri_transaction_payee>>();
            services.AddScoped<ISortHelper<poste_transaction_payee>, SortHelper<poste_transaction_payee>>();
            services.AddScoped<ISortHelper<viamobile_transaction_payee>, SortHelper<viamobile_transaction_payee>>();
            services.AddScoped<ISortHelper<APH_ADT_AnnulataionDesTransacations>, SortHelper<APH_ADT_AnnulataionDesTransacations>>();
            services.AddScoped<ISortHelper<APH_VER_VersementEnRoute>, SortHelper<APH_VER_VersementEnRoute>>();
            services.AddScoped<ISortHelper<APH_CJF_CalendrierJourFerie>, SortHelper<APH_CJF_CalendrierJourFerie>>();

            services.AddScoped<IRepositoryWrapper, RepositoryWrapper>();
        }
    }
}
