﻿using AutoMapper;
using Entities.DataTransferObjects;
using Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APH_Advans
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<POC_OWN_Owner, POC_OWN_OwnerDTO>();
            CreateMap<POC_ACC_Account, POC_ACC_AccountDTO>();
            CreateMap<POC_OWN_OwnerForCreationDto, POC_OWN_Owner>();
            CreateMap<POC_OWN_OwnerForUpdateDto, POC_OWN_Owner>();
        }
    }
}
