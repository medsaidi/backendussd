﻿using AutoMapper;
using Contracts;
using Entities.DataTransferObjects;
using Entities.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APH_Advans.Controllers
{

    [Route("api/VersementEnRoute")]
    [ApiController]
    //[Authorize]
    public class APH_VER_VersementEnRouteController : ControllerBase
    {
        private ILoggerManager _logger;
        private IRepositoryWrapper _repository;
        private IMapper _mapper;
        public APH_VER_VersementEnRouteController(ILoggerManager logger, IRepositoryWrapper repository, IMapper mapper)
        {
            _logger = logger;
            _repository = repository;
            _mapper = mapper;
        }

        [HttpPost]
        public IActionResult GetVersement([FromBody] APH_VER_VersementEnRouteParameter Parameters)
        {
           
            var reternedList = _repository.VersementEnRoutes.GetVersementEnRoutes(Parameters);
            var metadata = new
            {
                reternedList.TotalCount,
                reternedList.PageSize,
                reternedList.CurrentPage,
                reternedList.TotalPages,
                reternedList.HasNext,
                reternedList.HasPrevious
            };
            Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(metadata));
            _logger.LogInfo($"Returned {reternedList.TotalCount} owners from database.");

            return Ok(reternedList);
        }

        

      [HttpGet("{id}", Name = "VersementById")]
        public IActionResult VersementById(int id)
        {
            try
            {
                var suspendus = _repository.VersementEnRoutes.GetVersementEnRoutesById(id);
                if (suspendus == null)
                {
                    _logger.LogError($" Suspendus with id: {id}, hasn't been found in db.");
                    return NotFound();
                }
                else
                {
                    _logger.LogInfo($"Suspendus owner with id: {id}");

                    return Ok(suspendus);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetOwnerById action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }


       
        [HttpPut]
        public IActionResult UpdateVersement([FromBody] APH_VER_VersementEnRouteParameter[] Parameters)
        {

            try
            {
                

                 _repository.VersementEnRoutes.UpdateArray(Parameters);
                _repository.Save();
                return Ok();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside UpdateOwner action: {ex.Message}");
                return StatusCode(500, ex.Message);
            }
        }



    }
}