﻿using Contracts;

using Entities;
using Entities.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using MailKit.Net.Smtp;
using MimeKit;
using System.Net.Http.Formatting;
using System.Net;
using MailKit;
using MimeKit.Text;
using System.Net.Http;
using System.Net.Http.Headers;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;

namespace APH_Advans.Controllers
{

    [Route("api/authenticate")]
    [ApiController]
    public class AuthenticateController : ControllerBase
    {
        private readonly UserManager<ApplicationUser> userManager;
        private readonly RoleManager<IdentityRole> roleManager;
        private readonly IConfiguration _configuration;
        private ILoggerManager _logger;
        private RepositoryContext _repositoryContext;


        public AuthenticateController(ILoggerManager logger, UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager, IConfiguration configuration, RepositoryContext repositoryContext)
        {
            _logger = logger;
            this.userManager = userManager;
            this.roleManager = roleManager;
            _configuration = configuration;
            _repositoryContext = repositoryContext;

        }

        [HttpPost]
        [Route("login")]
        public async Task<IActionResult> Login([FromBody] LoginModel model)
        {
            try
            {

                _logger.LogInfo($"debut login");
                //var user = await userManager.FindByNameAsync(model.Username); 
                //if (user != null)
                //{
                //bool auth = AuthenticateUser(model.Username, model.Password);


                //if (auth)
                //{
                //    var user = await userManager.FindByNameAsync(model.Username);

                //    if(user==null)
                //    {
                //        RegisterModel registremodel = GetUser(model.Username); 
                //        registremodel.Password = model.Password;
                //       var registre = await Register(registremodel);
                //        user = await userManager.FindByNameAsync(model.Username);
                //    }
                //    var userRoles = await userManager.GetRolesAsync(user);

                //    var authClaims = new List<Claim>
                //    {
                //        new Claim(ClaimTypes.Name, model.Username),
                //        new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                //    };

                //    foreach (var userRole in userRoles)
                //    {
                //        authClaims.Add(new Claim(ClaimTypes.Role, userRole));
                //    }

                    var authSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JWT:Secret"]));

                    var token = new JwtSecurityToken(
                        issuer: _configuration["JWT:ValidIssuer"],
                        audience: _configuration["JWT:ValidAudience"],
                        expires: DateTime.Now.AddHours(3)
                        //claims: authClaims,
                        //signingCredentials: new SigningCredentials(authSigningKey, SecurityAlgorithms.HmacSha256)
                        );
                    _logger.LogInfo($"test logger published");
                    return Ok(new
                    {
                        token = new JwtSecurityTokenHandler().WriteToken(token),
                        expiration = token.ValidTo,
                        role = "PasDeRole"
                    }); ;
             //   }
                //}
                return Unauthorized();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                throw ex;
            }
        }

        [HttpGet("GroupUser")]
        public string Groups([FromQuery] string userName)
        {
            UserPrincipal user = null;
            string gr = "";
            int i = 0;
            bool ok = false;
            PrincipalContext ctx = new PrincipalContext(ContextType.Domain, "advans.ad", "OU = Users, OU = Users, OU = TN, OU = ADVANS, DC = advans, DC = ad");
            {
                if ((user = UserPrincipal.FindByIdentity(ctx, userName)) != null)
                {
                    PrincipalSearchResult<Principal> groups = user.GetGroups();

                    while (groups.Count() > i && ok == false)
                    {
                        if (groups.ElementAt(i).Name.Contains("BACKOFFICE"))
                        {
                            gr = "BO";
                            ok = true;
                        }
                        else { i++; }
                    }

                }
            }
            return gr;
        }

        [HttpGet("AuthAD")]
        public bool AuthenticateUser([FromQuery] string userName, string password)
        {
            bool ret = false;

            try
            {
                DirectoryEntry de = new DirectoryEntry("LDAP://OU=Users,OU=Users,OU=TN,OU=ADVANS,DC=advans,DC=ad", userName, password);
                DirectorySearcher dsearch = new DirectorySearcher(de);
                SearchResult results = null;

                results = dsearch.FindOne();
               

                ret = true;
            }
            catch (Exception ex)
            {
                ret = true;
            }

            return ret;
        }

        [HttpGet("GetUser")]
        public RegisterModel GetUser([FromQuery] string userName  )
        {


            RegisterModel mod = new RegisterModel();
            string DomainPath = "LDAP://OU=Users,OU=Users,OU=TN,OU=ADVANS,DC=advans,DC=ad";
            DirectoryEntry searchRoot = new DirectoryEntry(DomainPath);
            DirectorySearcher search = new DirectorySearcher(searchRoot);
            search.Filter = "(&(objectClass=user)(objectCategory=person))";
            search.PropertiesToLoad.Add("samaccountname");
            search.PropertiesToLoad.Add("mail");
            search.PropertiesToLoad.Add("telephoneNumber");
            SearchResult result;
            SearchResultCollection resultCol = search.FindAll();
            if (resultCol != null)
            {
                for (int counter = 0; counter < resultCol.Count; counter++)
                {
                    result = resultCol[counter];
                    if (result.Properties.Contains("samaccountname") &&
                             result.Properties.Contains("mail") )
                    {
                        if ((String)result.Properties["samaccountname"][0] == userName)
                        {
                           
                            mod.Email = (String)result.Properties["mail"][0];
                            mod.Phone = (String)result.Properties["telephoneNumber"][0];
                            mod.Username = (String)result.Properties["samaccountname"][0];
                            //mail = (String)result.Properties["mail"][0] +"  " + (String)result.Properties["telephoneNumber"][0];

                        }


                    }
                }
            }
            return mod;
        }




        [HttpGet("GeneratePassword")]
        public string GeneratePassword()
        {
            // var options = _userManager.Options.Password;

            int length = 10;

            bool nonAlphanumeric = true;
            bool digit = true;
            bool lowercase = true;
            bool uppercase = true;

            StringBuilder password = new StringBuilder();
            Random random = new Random();

            while (password.Length < length)
            {
                char c = (char)random.Next(32, 126);

                password.Append(c);

                if (char.IsDigit(c))
                    digit = false;
                else if (char.IsLower(c))
                    lowercase = false;
                else if (char.IsUpper(c))
                    uppercase = false;
                else if (!char.IsLetterOrDigit(c))
                    nonAlphanumeric = false;
            }

            if (nonAlphanumeric)
                password.Append((char)random.Next(33, 48));
            if (digit)
                password.Append((char)random.Next(48, 58));
            if (lowercase)
                password.Append((char)random.Next(97, 123));
            if (uppercase)
                password.Append((char)random.Next(65, 91));

            return password.ToString();
        }

        [HttpGet("Chaine")]
        public string Chaine(string mail, string pwd)
        {
            return "Bonjour \n" +
                "Votre login est : " + mail +
                "\n" +
                "Votre mot de passe est : +" + pwd +
                "\n" +
                "Bien a vous";


        }


        [HttpPost("mail")]
        public void SendMail(string mail, string pwd)
        {
            var email = new MimeMessage();
            email.From.Add(MailboxAddress.Parse("shajri@advanstunisie.com"));
            email.To.Add(MailboxAddress.Parse(mail));
            email.Subject = "User APH";
            email.Body = new TextPart(TextFormat.Plain)
            {
                Text = "Bonjour \n" +
                "Votre login est :\n" + mail +
                "\n" +
                "Votre mot de passe est : \n" + pwd +
                "\n" +
                "Bien a vous"
            };

            // send email
            using var smtp = new SmtpClient();
            smtp.Connect("10.80.1.80", 25, MailKit.Security.SecureSocketOptions.None);
            // smtp.Authenticate("[USERNAME]", "[PASSWORD]");
            smtp.Send(email);
            smtp.Disconnect(true);
        }


        [HttpPost("GetUsers")]
        public IActionResult GetUsers()
        {

            return Ok(_repositoryContext.User.ToList());

        }

        [HttpPost]
        [Route("register")]
        public async Task<IActionResult> Register([FromBody] RegisterModel model)
        {
            var userExists = await userManager.FindByNameAsync(model.Username);
            if (userExists != null)
                return StatusCode(StatusCodes.Status500InternalServerError, new Response { Status = "Error", Message = "User already exists!" });

            ApplicationUser user = new ApplicationUser()
            {
                Email = model.Email,
                SecurityStamp = Guid.NewGuid().ToString(),
                UserName = model.Username,
                PhoneNumber = model.Phone

            };
            model.Password = GeneratePassword();
            var result = await userManager.CreateAsync(user, model.Password);
            if (!result.Succeeded)
                return StatusCode(StatusCodes.Status500InternalServerError, new Response { Status = "Error", Message = "User creation failed! Please check user details and try again." });
            //SendMail(model.Email, model.Password);
            return Ok(new Response { Status = "Success", Message = "User created successfully!               " + model.Password });
        }

        [HttpPost]
        [Route("register-admin")]
        public async Task<IActionResult> RegisterAdmin([FromBody] RegisterModel model)
        {
            var userExists = await userManager.FindByNameAsync(model.Username);
            if (userExists != null)
                return StatusCode(StatusCodes.Status500InternalServerError, new Response { Status = "Error", Message = "User already exists!" });

            ApplicationUser user = new ApplicationUser()
            {
                Email = model.Email,
                SecurityStamp = Guid.NewGuid().ToString(),
                UserName = model.Username
            };
            var result = await userManager.CreateAsync(user, model.Password);
            if (!result.Succeeded)
                return StatusCode(StatusCodes.Status500InternalServerError, new Response { Status = "Error", Message = "User creation failed! Please check user details and try again." });

            if (!await roleManager.RoleExistsAsync(UserRoles.Admin))
                await roleManager.CreateAsync(new IdentityRole(UserRoles.Admin));
            if (!await roleManager.RoleExistsAsync(UserRoles.User))
                await roleManager.CreateAsync(new IdentityRole(UserRoles.User));

            if (await roleManager.RoleExistsAsync(UserRoles.Admin))
            {
                await userManager.AddToRoleAsync(user, UserRoles.Admin);
            }

            return Ok(new Response { Status = "Success", Message = "User created successfully!" });
        }
    }
}
