﻿using AutoMapper;
using Contracts;
using Entities.DataTransferObjects;
using Entities.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APH_Advans.Controllers
{

    [Route("api/Doublons")]
    [ApiController]
    // [Authorize]
    public class APH_RMD_RapprochementManuelleDoublonsController : ControllerBase
    {
        private ILoggerManager _logger;
        private IRepositoryWrapper _repository;
        private IMapper _mapper;
        public APH_RMD_RapprochementManuelleDoublonsController(ILoggerManager logger, IRepositoryWrapper repository, IMapper mapper)
        {
            _logger = logger;
            _repository = repository;
            _mapper = mapper;
        }

        [HttpPost]
        public IActionResult GetDoublons([FromBody] APH_RMD_RapprochementManuelleDoublonsParameters Parameters)
        {
            
            var reternedList = _repository.Doublons.GetDoublons(Parameters);
            var metadata = new
            {
                reternedList.TotalCount,
                reternedList.PageSize,
                reternedList.CurrentPage,
                reternedList.TotalPages,
                reternedList.HasNext,
                reternedList.HasPrevious
            };
            Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(metadata));
            _logger.LogInfo($"Returned {reternedList.TotalCount} owners from database.");

            return Ok(reternedList);
        }

        [HttpGet("{id}", Name = "doublonsById")]
        public IActionResult GestDoublonsById(int id)
        {
            try
            {
                //var doublons = _repository.Suspendus.GetDoublonsById;
                var doublons = _repository.Doublons.GetDoublonsById(id);
                if (doublons == null)
                {
                    _logger.LogError($" Suspendus with id: {id}, hasn't been found in db.");
                    return NotFound();
                }
                else
                {
                    _logger.LogInfo($"Suspendus owner with id: {id}");

                    return Ok(doublons);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetOwnerById action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        [HttpPut]
        public   IActionResult UpdatbyDoubonsObject([FromBody] APH_RMD_RapprochementManuelleDoublonsParameters Parameters)
        {

            try
            {
                var douEntity = _repository.Doublons.GetDoublonsById(Parameters.RMD_ID);
                if (douEntity == null)
                {
                    _logger.LogError("Owner object sent from client is null.");
                    return BadRequest("Owner object is null");
                }
                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid owner object sent from client.");
                    return BadRequest("Invalid model object");
                }

                if (douEntity == null)
                {
                    _logger.LogError($"Owner with id: {Parameters.RMD_ID}, hasn't been found in db.");
                    return NotFound();
                }
                //_mapper.Map(owner, ownerEntity);

                
                _repository.Doublons.UpdatbyDoubonsObject(Parameters);
                _repository.Save();
                return Ok();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside UpdateOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }


        [HttpPost("verificationDoublon")]
        public IActionResult verificationDoublon([FromBody] APH_RMD_RapprochementManuelleDoublonsParameters suspendus)
        {
            try
            {
                var response = _repository.Doublons.verificationDoublon(suspendus);
                //var createdOwner = _mapper.Map<POC_OWN_OwnerDTO>(ownerEntity);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside CreateOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }





    }
}
