﻿using AutoMapper;
using Contracts;
using Entities.DataTransferObjects;
using Entities.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APH_Advans.Controllers
{

    [Route("api/Suspendus")]
    [ApiController]
    //[Authorize]
    public class APH_RDS_RapprochementDesSuspendusController : ControllerBase
    {
        private ILoggerManager _logger;
        private IRepositoryWrapper _repository;
        private IMapper _mapper;
        public APH_RDS_RapprochementDesSuspendusController(ILoggerManager logger, IRepositoryWrapper repository, IMapper mapper)
        {
            _logger = logger;
            _repository = repository;
            _mapper = mapper;
        }

        [HttpPost]
        public IActionResult GetSuspendus([FromBody] APH_RDS_RapprochementDesSuspendusParameters Parameters)
        {
            //if (!Parameters.ValidYearRange)
            //{
            //    return BadRequest("Max year of birth cannot be less than min year of birth");
            //}
            var reternedList = _repository.Suspendus.GetSuspendus(Parameters);
            var metadata = new
            {
                reternedList.TotalCount,
                reternedList.PageSize,
                reternedList.CurrentPage,
                reternedList.TotalPages,
                reternedList.HasNext,
                reternedList.HasPrevious
            };
            Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(metadata));
            _logger.LogInfo($"Returned {reternedList.TotalCount} owners from database.");

            return Ok(reternedList);
        }



        [HttpGet("{id}", Name = "SuspendusById")]
        public IActionResult GestSuspendusById(int id)
        {
            try
            {
                var suspendus = _repository.Suspendus.GetSuspendusById(id);
                if (suspendus == null)
                {
                    _logger.LogError($" Suspendus with id: {id}, hasn't been found in db.");
                    return NotFound();
                }
                else
                {
                    _logger.LogInfo($"Suspendus owner with id: {id}");

                    return Ok(suspendus);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetOwnerById action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }


        //[HttpPost]
        //public IActionResult CreateSuspendus([FromBody] APH_RDS_RapprochementDesSuspendus suspendus)
        //{
        //    try
        //    {
        //        if (suspendus == null)
        //        {
        //            _logger.LogError("Owner object sent from client is null.");
        //            return BadRequest("Owner object is null");
        //        }
        //        if (!ModelState.IsValid)
        //        {
        //            _logger.LogError("Invalid owner object sent from client.");
        //            return BadRequest("Invalid model object");
        //        }

        //        _repository.Suspendus.CreateSuspendus(suspendus);
        //        _repository.Save();
        //        //var createdOwner = _mapper.Map<POC_OWN_OwnerDTO>(ownerEntity);
        //        return CreatedAtRoute("SuspendusByID", new { id = suspendus.RDS_ID }, suspendus);
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.LogError($"Something went wrong inside CreateOwner action: {ex.Message}");
        //        return StatusCode(500, "Internal server error");
        //    }
        //}

        //[HttpPut("{id}")]
        //public IActionResult UpdateSuspendus(int id , [FromBody] APH_RDS_RapprochementDesSuspendus suspendus)
        //{
        //    try
        //    {
        //        if (suspendus == null)
        //        {
        //            _logger.LogError("Owner object sent from client is null.");
        //            return BadRequest("Owner object is null");
        //        }
        //        if (!ModelState.IsValid)
        //        {
        //            _logger.LogError("Invalid owner object sent from client.");
        //            return BadRequest("Invalid model object");
        //        }
        //        var susEntity = _repository.Suspendus.GetSuspendusById(id);
        //        if (susEntity == null)
        //        {
        //            _logger.LogError($"Owner with id: {id}, hasn't been found in db.");
        //            return NotFound();
        //        }
        //        //_mapper.Map(owner, ownerEntity);

        //        _repository.Suspendus.UpdateSuspendus(suspendus);
        //        _repository.Save();
        //        return NoContent();
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.LogError($"Something went wrong inside UpdateOwner action: {ex.Message}");
        //        return StatusCode(500, "Internal server error");
        //    }
        //}


        [HttpPut]
        public IActionResult UpdateSuspendusobject([FromBody] APH_RDS_RapprochementDesSuspendusParameters Parameters)
        {

            try
            {
                var susEntity = _repository.Suspendus.GetSuspendusById(Parameters.RDS_ID);
                if (susEntity == null)
                {
                    _logger.LogError("Owner object sent from client is null.");
                    return BadRequest("Owner object is null");
                }
                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid owner object sent from client.");
                    return BadRequest("Invalid model object");
                }

                if (susEntity == null)
                {
                    _logger.LogError($"Owner with id: {Parameters.RDS_ID}, hasn't been found in db.");
                    return NotFound();
                }
                //_mapper.Map(owner, ownerEntity);

                  _repository.Suspendus.UpdateSuspendusObject(Parameters);
                _repository.Save();
                return Ok();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside UpdateOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        [HttpPost("VerificationCohereance")]
        public IActionResult VerificationCohereance([FromBody] APH_RDS_RapprochementDesSuspendusParameters suspendus)
        {
            try
            {
                var response = _repository.Suspendus.VerificationCoherence(suspendus);
                //var createdOwner = _mapper.Map<POC_OWN_OwnerDTO>(ownerEntity);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside CreateOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

    }
}