﻿using AutoMapper;
using Contracts;
using Entities.DataTransferObjects;
using Entities.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APH_Advans.Controllers
{
    [Route("api/Incoherences")]
    [ApiController]
   // [Authorize]
    public class APH_RMI_RapprochementManuelleIncohernceController : ControllerBase
    {
        private ILoggerManager _logger;
        private IRepositoryWrapper _repository;
        private IMapper _mapper;
        public APH_RMI_RapprochementManuelleIncohernceController(ILoggerManager logger, IRepositoryWrapper repository, IMapper mapper)
        {
            _logger = logger;
            _repository = repository;
            _mapper = mapper;
        }
        [HttpPost]
        public IActionResult GetIncohenrences([FromBody] APH_RMI_RapprochementManuelleIncohernceParameters Parameters)
        {
            //if (!Parameters.ValidYearRange)
            //{
            //    return BadRequest("Max year of birth cannot be less than min year of birth");
            //}
            try
            {
                var reternedList = _repository.Incoherences.GetIncoherence(Parameters);
                var metadata = new
                {
                    reternedList.TotalCount,
                    reternedList.PageSize,
                    reternedList.CurrentPage,
                    reternedList.TotalPages,
                    reternedList.HasNext,
                    reternedList.HasPrevious
                };
                Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(metadata));
                
                return Ok(reternedList);
            }
            catch (Exception ex)
            {

                _logger.LogInfo($"expp {ex.Message}");
                _logger.LogError($"expp {ex.Message}");
                throw;
            }
            
        }
        [HttpGet("CorrectionAutomatique")]
        public IActionResult GetCorrectionAutomatique([FromQuery] APH_RMI_RapprochementManuelleIncohernceParameters Parameters)
        {
            var reternedList = _repository.Incoherences.GetCorrectionAutomatique(Parameters);
            _repository.Save();
            //Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(metadata));
            _logger.LogInfo($"Returned {reternedList.Count} owners from database.");
            return Ok(reternedList);
        }
        [HttpPost("SaveSuggestedCorrection")]
        public IActionResult PostSaveSuggestedCorrection([FromBody] APH_RMI_RapprochementManuelleIncohernceParameters Parameters)
        {
           // var reternedList = _repository.Incoherences.GetCorrectionAutomatique(Parameters);
            //_repository.Save();
            ////Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(metadata));
            //_logger.LogInfo($"Returned {reternedList.Count} owners from database.");
            return Ok(null);
        }


        [HttpPut("CorrectionAuto")]
        public IActionResult UpdatbyDoubonsObject([FromBody] APH_RMI_RapprochementManuelleIncohernceParameters Parameters)
        {

            try
            {
                var douEntity = _repository.Incoherences.GetDoublonsById(Parameters.RMI_ID);
                if (douEntity == null)
                {
                    _logger.LogError("Owner object sent from client is null.");
                    return BadRequest("Owner object is null");
                }
                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid owner object sent from client.");
                    return BadRequest("Invalid model object");
                }

                if (douEntity == null)
                {
                    _logger.LogError($"Owner with id: {Parameters.RMI_ID}, hasn't been found in db.");
                    return NotFound();
                }
                //_mapper.Map(owner, ownerEntity);


                _repository.Incoherences.UpdateIncoherence(Parameters);
                _repository.Save();
                return Ok();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside UpdateOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        [HttpPut("CorrectionManuelle")]
        public IActionResult UpdatbyDoubonsObjectManuelle([FromBody] APH_RMI_RapprochementManuelleIncohernceParameters Parameters)
        {

            try
            {
                var douEntity = _repository.Incoherences.GetDoublonsById(Parameters.RMI_ID);
                if (douEntity == null)
                {
                    _logger.LogError("Owner object sent from client is null.");
                    return BadRequest("Owner object is null");
                }
                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid owner object sent from client.");
                    return BadRequest("Invalid model object");
                }

                if (douEntity == null)
                {
                    _logger.LogError($"Owner with id: {Parameters.RMI_ID}, hasn't been found in db.");
                    return NotFound();
                }
                //_mapper.Map(owner, ownerEntity);


                _repository.Incoherences.UpdateIncoherenceManuelle(Parameters);
                _repository.Save();
                return Ok();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside UpdateOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }


        [HttpGet("{id}", Name = "IncoById")]
        public IActionResult IncoById(int id)
        {
            try
            {
                var suspendus = _repository.Incoherences.GetDoublonsById(id);
                if (suspendus == null)
                {
                    _logger.LogError($" Suspendus with id: {id}, hasn't been found in db.");
                    return NotFound();
                }
                else
                {
                    _logger.LogInfo($"Suspendus owner with id: {id}");

                    return Ok(suspendus);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetOwnerById action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

    }
}
