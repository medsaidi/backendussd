﻿using System;
using AutoMapper;
using Contracts;
using Entities.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace USSD_BackOffice.Controllers
{
    [Route("api/Client")]
    [ApiController]
    public class ClientController : ControllerBase
    {
        private readonly ILoggerManager _logger;
        private readonly IRepositoryWrapper _repository;
        private IMapper _mapper;

        public ClientController(ILoggerManager logger, IRepositoryWrapper repository, IMapper mapper)
        {
            _logger = logger;
            _repository = repository;
            _mapper = mapper;
        }

        [HttpPost]
        public IActionResult GetClients([FromBody] ClientParameters parameters)
        {
            var returnedList = _repository.Clients.Get(parameters);
            var metadata = new
            {
                returnedList.TotalCount,
                returnedList.PageSize,
                returnedList.CurrentPage,
                returnedList.TotalPages,
                returnedList.HasNext,
                returnedList.HasPrevious
            };
            Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(metadata));
            _logger.LogInfo($"Returned {returnedList.TotalCount} owners from database.");

            return Ok(returnedList);
        }

        [HttpPost("TransactionsClient")]
        public IActionResult GetTransactionsClient([FromBody] TransactionsClientsParameters parameters)
        {
            var returnedList = _repository.TransactionsClients.GetTransactionsClients(parameters);
            var metadata = new
            {
                returnedList.TotalCount,
                returnedList.PageSize,
                returnedList.CurrentPage,
                returnedList.TotalPages,
                returnedList.HasNext,
                returnedList.HasPrevious
            };
            Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(metadata));
            _logger.LogInfo($"Returned {returnedList.TotalCount} owners from database.");

            return Ok(returnedList);
        }

        [HttpGet("GetAllBranch")]
        public IActionResult GetAllBranch()
        {
            try
            {
                var client = _repository.Clients.GetAllBranch();

                return Ok(client);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetOwnerById action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }


        [HttpPost("ClientInfos")]
        public IActionResult GetInfo([FromBody] ClientInfosParameters parameters)
        {
            var returnedList = _repository.ClientInfos.GetClientInfos(parameters);
            var metadata = new
            {
                returnedList.TotalCount,
                returnedList.PageSize,
                returnedList.CurrentPage,
                returnedList.TotalPages,
                returnedList.HasNext,
                returnedList.HasPrevious
            };
            Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(metadata));
            _logger.LogInfo($"Returned {returnedList.TotalCount} owners from database.");

            return Ok(returnedList);
        }

        [HttpPost("ResetPIN")]
        public IActionResult ResetPin(ClientInfosParameters parameters)
        {
            var returnedList = _repository.Clients.ResetPin(parameters.IdClient);

            return Ok(returnedList);
        }

        [HttpPost("ExportClients")]
        public IActionResult ExportClients([FromBody] ClientParameters parameters)
        {
            var returnedList = _repository.Clients.GetToExport(parameters);
            var stream = _repository.Clients.MemoryStream(returnedList);

            return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                "ListClients.xlsx");
        }


        [HttpPost("ExportTransactionsClients")]
        public IActionResult ExportTransactionsClients([FromBody] TransactionsClientsParameters parameters)
        {
            var returnedList = _repository.TransactionsClients.GetTransactionsClientsToExport(parameters);
            var stream = _repository.TransactionsClients.MemoryStream(returnedList);

            return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                "ListTransactionsClients.xlsx");
        }
    }
}