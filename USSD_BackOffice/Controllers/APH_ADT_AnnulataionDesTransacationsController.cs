﻿using AutoMapper;
using Contracts;
using Entities.DataTransferObjects;
using Entities.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APH_Advans.Controllers
{

    [Route("api/Annulataion")]
    [ApiController]
    //[Authorize]
    public class APH_ADT_AnnulataionDesTransacationsController : ControllerBase
    {
        private ILoggerManager _logger;
        private IRepositoryWrapper _repository;
        private IMapper _mapper;
        public APH_ADT_AnnulataionDesTransacationsController(ILoggerManager logger, IRepositoryWrapper repository, IMapper mapper)
        {
            _logger = logger;
            _repository = repository;
            _mapper = mapper;
        }

        [HttpPost]
        public IActionResult GetAnnulation([FromBody] APH_ADT_AnnulataionDesTransacationsParameters Parameters)
        {
           
            var reternedList = _repository.Annulation.GetAnnulation(Parameters);
            var metadata = new
            {
                reternedList.TotalCount,
                reternedList.PageSize,
                reternedList.CurrentPage,
                reternedList.TotalPages,
                reternedList.HasNext,
                reternedList.HasPrevious
            };
            Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(metadata));
            _logger.LogInfo($"Returned {reternedList.TotalCount} owners from database.");

            return Ok(reternedList);
        }


        [HttpGet("{id}", Name = "AnnulationById")]
        public IActionResult GestAnnulationById(int id)
        {
            try
            {
                //var doublons = _repository.Suspendus.GetDoublonsById;
                var Annulation = _repository.Annulation.GetAnnulationById(id);
                if (Annulation == null)
                {
                    _logger.LogError($" Suspendus with id: {id}, hasn't been found in db.");
                    return NotFound();
                }
                else
                {
                    _logger.LogInfo($"Suspendus owner with id: {id}");

                    return Ok(Annulation);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetOwnerById action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        //[HttpPut]
        //public IActionResult UpdateAnnulationObject([FromBody] APH_ADT_AnnulataionDesTransacationsParameters Parameters)
        //{

        //    try
        //    {
        //        var susEntity = _repository.Annulation.GetAnnulationById(Parameters.ADT_ID);
        //        if (susEntity == null)
        //        {
        //            _logger.LogError("Owner object sent from client is null.");
        //            return BadRequest("Owner object is null");
        //        }
        //        if (!ModelState.IsValid)
        //        {
        //            _logger.LogError("Invalid owner object sent from client.");
        //            return BadRequest("Invalid model object");
        //        }

        //        if (susEntity == null)
        //        {
        //            _logger.LogError($"Owner with id: {Parameters.ADT_ID}, hasn't been found in db.");
        //            return NotFound();
        //        }
        //        //_mapper.Map(owner, ownerEntity);
        //        UpdateArray

        //        var sus = _repository.Annulation.UpdateAnnulationObject(Parameters);
        //        _repository.Save();
        //        return Ok(sus);
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.LogError($"Something went wrong inside UpdateOwner action: {ex.Message}");
        //        return StatusCode(500, "Internal server error");
        //    }
        //}


        [HttpPut]
        public IActionResult UpdateAnnulationObject([FromBody] APH_ADT_AnnulataionDesTransacationsParameters[] Parameters)
        {

            try
            {


                _repository.Annulation.UpdateArray(Parameters);
                _repository.Save();
                return Ok();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside UpdateOwner action: {ex.Message}");
                return StatusCode(500, ex.Message);
            }
        }

    }
}