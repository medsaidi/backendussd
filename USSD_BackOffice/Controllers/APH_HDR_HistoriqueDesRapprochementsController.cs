﻿using AutoMapper;
using Contracts;
using Entities.DataTransferObjects;
using Entities.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APH_Advans.Controllers
{

    [Route("api/Historique")]
    [ApiController]
    //[Authorize]
    public class APH_HDR_HistoriqueDesRapprochementsController : ControllerBase
    {
        private ILoggerManager _logger;
        private IRepositoryWrapper _repository;
        private IMapper _mapper;
        public APH_HDR_HistoriqueDesRapprochementsController(ILoggerManager logger, IRepositoryWrapper repository, IMapper mapper)
        {
            _logger = logger;
            _repository = repository;
            _mapper = mapper;
        }

        [HttpPost]
        public IActionResult GetHistorique([FromBody] APH_HDR_HistoriqueDesRapprochementsParameters Parameters)
        {
             
            var reternedList = _repository.Historique.GetHistorique(Parameters);
            var metadata = new
            {
                reternedList.TotalCount,
                reternedList.PageSize,
                reternedList.CurrentPage,
                reternedList.TotalPages,
                reternedList.HasNext,
                reternedList.HasPrevious
            };
            Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(metadata));
            _logger.LogInfo($"Returned {reternedList.TotalCount} owners from database.");

            return Ok(reternedList);
        }


    }
}