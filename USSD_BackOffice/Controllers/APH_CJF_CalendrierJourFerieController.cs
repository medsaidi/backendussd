﻿using AutoMapper;
using Contracts;
using Entities.DataTransferObjects;
using Entities.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APH_Advans.Controllers
{

    [Route("api/Ferie")]
    [ApiController]
    //[Authorize]
    public class APH_CJF_CalendrierJourFerieController : ControllerBase
    {
        private ILoggerManager _logger;
        private IRepositoryWrapper _repository;
        private IMapper _mapper;
        public APH_CJF_CalendrierJourFerieController(ILoggerManager logger, IRepositoryWrapper repository, IMapper mapper)
        {
            _logger = logger;
            _repository = repository;
            _mapper = mapper;
        }

        [HttpPost]
        public IActionResult GetFerie([FromBody] APH_CJF_CalendrierJourFerieParameters Parameters)
        {
            
            var reternedList = _repository.CalendrierJourFerie.GetJourFeries(Parameters);
            var metadata = new
            {
                reternedList.TotalCount,
                reternedList.PageSize,
                reternedList.CurrentPage,
                reternedList.TotalPages,
                reternedList.HasNext,
                reternedList.HasPrevious
            };
            Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(metadata));
            _logger.LogInfo($"Returned {reternedList.TotalCount} owners from database.");

            return Ok(reternedList);
        }



        [HttpGet("{id}", Name = "JoursFerieByName")]
        public IActionResult GestSuspendusById(int id)
        {
            try
            {
                var suspendus = _repository.CalendrierJourFerie.GetJourFeriesById(id);
                if (suspendus == null)
                {
                    _logger.LogError($" Suspendus with id: {id}, hasn't been found in db.");
                    return NotFound();
                }
                else
                {
                    _logger.LogInfo($"Suspendus owner with id: {id}");

                    return Ok(suspendus);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetOwnerById action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }


        [HttpPost ("CreateJoursFerie")]
        public IActionResult CreateSuspendus([FromBody] APH_CJF_CalendrierJourFerieParameters suspendus)
        {
            try
            {
                if (suspendus == null)
                {
                    _logger.LogError("Owner object sent from client is null.");
                    return BadRequest("Owner object is null");
                }
                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid owner object sent from client.");
                    return BadRequest("Invalid model object");
                }

                _repository.CalendrierJourFerie.CreateJourFerie(suspendus);
                _repository.Save();
                //var createdOwner = _mapper.Map<POC_OWN_OwnerDTO>(ownerEntity);
                return CreatedAtRoute("SuspendusByID", new { id = suspendus.CJF_ID }, suspendus);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside CreateOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }




        [HttpPut]
        public IActionResult UpdateSuspendusobject([FromBody] APH_CJF_CalendrierJourFerieParameters Parameters)
        {

            try
            {
                var susEntity = _repository.CalendrierJourFerie.GetJourFeriesById(Parameters.CJF_ID);
                if (susEntity == null)
                {
                    _logger.LogError("Owner object sent from client is null.");
                    return BadRequest("Owner object is null");
                }
                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid owner object sent from client.");
                    return BadRequest("Invalid model object");
                }

                if (susEntity == null)
                {
                    _logger.LogError($"Owner with id: {Parameters.CJF_ID}, hasn't been found in db.");
                    return NotFound();
                }
                //_mapper.Map(owner, ownerEntity);

                _repository.CalendrierJourFerie.UpdateJourFerie(Parameters);
                _repository.Save();
                return Ok();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside UpdateOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }



    }
}