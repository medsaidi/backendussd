﻿using AutoMapper;
using Contracts;
using Entities.DataTransferObjects;
using Entities.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APH_Advans.Controllers
{
    [Route("api/monitoring")]
    [ApiController]
    // [Authorize]
    public class APH_V_Monitoring_PartenaireController : ControllerBase
    {
        private ILoggerManager _logger;
        private IRepositoryWrapper _repository;
        private IMapper _mapper;
        public APH_V_Monitoring_PartenaireController(ILoggerManager logger, IRepositoryWrapper repository, IMapper mapper)
        {
            _logger = logger;
            _repository = repository;
            _mapper = mapper;
        }
        [HttpPost("GetMonitoring")]
        public IActionResult GetMonitoring([FromBody] APH_V_Monitoring_PartenaireParameters Parameters)
        {
            //if (!Parameters.ValidYearRange)
            //{
            //    return BadRequest("Max year of birth cannot be less than min year of birth");
            //}
            var reternedList = _repository.Monitoring.GetMonitoring(Parameters);
            IList<string> ListePartenaire = new List<string>();
            ListePartenaire.Add("Poste");ListePartenaire.Add("Poste-Reco");ListePartenaire.Add("Amen");ListePartenaire.Add("Amen-Reco");
            ListePartenaire.Add("ViaMobile");ListePartenaire.Add("ViaMobile-Reco");ListePartenaire.Add("AWB");ListePartenaire.Add("AWB-Reco");
            ListePartenaire.Add("UIB-IN");ListePartenaire.Add("UIB-OUT");
            foreach (var item in ListePartenaire)
            {
                if (!reternedList.Select(x => x.MOP_Source).Contains(item))
                {
                    APH_V_Monitoring_Partenaire partenerToAdd = new APH_V_Monitoring_Partenaire()
                    {
                        MOP_DateOperation = Parameters.MOP_DateOperation,
                        MOP_Source = item,
                        MOP_Total_Lignes = 0,
                        MOP_NEW = 0,
                        MOP_CORRECTED = 0,
                        MOP_SUCCESS = 0,
                        MOP_INCOHERENCE = 0,
                        MOP_DOUBLE = 0,
                        MOP_SUSPENDED = 0,
                        MOP_CASH_PAYMENT = 0,
                        MOP_ANNULATION = 0,
                        MOP_REJECTED = 0,
                        MOP_AmplitudeError = 0
                    };
                    reternedList.Add(partenerToAdd);
                }
            }
            var metadata = new
            {
                reternedList.Count,
            };
            Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(metadata));
            _logger.LogInfo($"Returned {reternedList.Count} owners from database.");
            return Ok(reternedList.OrderBy(x => x.MOP_Source));
        }
        [HttpPost("GetRejt")]
        public IActionResult Getrejet(APH_Extract_Parameters parameters)
        {
            //if (!Parameters.ValidYearRange)
            //{
            //    return BadRequest("Max year of birth cannot be less than min year of birth");
            //}
            return Ok(_repository.Monitoring.GetRejet(parameters));

        }
        [HttpPost("GetEpf")]
        public IActionResult GetEpf(APH_Extract_Parameters parameters)
        {
            //if (!Parameters.ValidYearRange)
            //{
            //    return BadRequest("Max year of birth cannot be less than min year of birth");
            //}
            return Ok(_repository.Monitoring.GetEpf(parameters));
        }
        [HttpPost("GetSuspendusHis")]
        public IActionResult GetSuspendusHis(APH_Extract_Parameters parameters)
        {
            //if (!Parameters.ValidYearRange)
            //{
            //    return BadRequest("Max year of birth cannot be less than min year of birth");
            //}
            return Ok(_repository.Monitoring.GetSuspendusHis(parameters));
        }
        [HttpPost("GetRompus")]
        public IActionResult GetRompus(APH_Extract_Parameters parameters)
        {
            //if (!Parameters.ValidYearRange)
            //{
            //    return BadRequest("Max year of birth cannot be less than min year of birth");
            //}
            return Ok(_repository.Monitoring.GetRompus(parameters));
        }
        [HttpPost("GetSuspendus")]
        public IActionResult GetSuspendus(APH_Extract_Parameters parameters)
        {
            //if (!Parameters.ValidYearRange)
            //{
            //    return BadRequest("Max year of birth cannot be less than min year of birth");
            //}
            return Ok(_repository.Monitoring.GetSuspendus(parameters));
        }
        [HttpPost("GetReco")]
        public IActionResult GetReconciliation(APH_Extract_Parameters parameters)
        {
            //if (!Parameters.ValidYearRange)
            //{
            //    return BadRequest("Max year of birth cannot be less than min year of birth");
            //}
            return Ok(_repository.Monitoring.GetReconciliation(parameters));
        }
        [HttpPost("GetSuccess")]
        public IActionResult GetTransferer(APH_Extract_Parameters parameters)
        {
            //if (!Parameters.ValidYearRange)
            //{
            //    return BadRequest("Max year of birth cannot be less than min year of birth");
            //}
            return Ok(_repository.Monitoring.GetSuccess(parameters));
        }
        [HttpPost]
        public IActionResult EnvoiAmplitude()
        {
            try
            {
                _repository.Monitoring.EnvoiAmplitude("Poste", "CORRECTED");
                _repository.Monitoring.EnvoiAmplitude("ViaMobile", "CORRECTED");
                _repository.Monitoring.EnvoiAmplitude("Attijeri", "CORRECTED");
                _repository.Monitoring.EnvoiAmplitude("Amen", "CORRECTED");

                _repository.Monitoring.EnvoiAmplitude("Attijeri", "SUSPENDED");
                _repository.Monitoring.EnvoiAmplitude("Amen", "SUSPENDED");

                _repository.Monitoring.EnvoiAmplitude("Attijeri", "SUSPENDED_CORRECTED");
                _repository.Monitoring.EnvoiAmplitude("Amen", "SUSPENDED_CORRECTED");
                _logger.LogInfo($" Envoie amplitude Done");
                return Ok();
            }
            catch (Exception ex)
            {
                _logger.LogInfo($"Err : {ex.Message} ");
                _logger.LogError($"Err : {ex.Message} ");
                return StatusCode(500, "Internal server error");
            }
        }
    }
}