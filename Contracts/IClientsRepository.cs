﻿using System.Collections.Generic;
using System.IO;
using Entities.Models;
using Repository;

namespace Contracts
{
    public interface IClientsRepository : IRepositoryBase<Client>
    {
        PagedList<Client> Get(ClientParameters parameters);
        List<string> GetAllBranch();
        Client[] GetToExport(ClientParameters parameters);
        bool ResetPin(int clientId);
        MemoryStream MemoryStream(Client[] returnedList);
    }
}