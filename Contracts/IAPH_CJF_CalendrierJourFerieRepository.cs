﻿using Entities.Models;
using Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace Contracts
{
    public interface IAPH_CJF_CalendrierJourFerieRepository : IRepositoryBase<APH_CJF_CalendrierJourFerie>
    {
      
        PagedList<APH_CJF_CalendrierJourFerie> GetJourFeries(APH_CJF_CalendrierJourFerieParameters Parameters);
        APH_CJF_CalendrierJourFerie GetJourFeriesById(int IdJours);
        void CreateJourFerie(APH_CJF_CalendrierJourFerieParameters suspendus);
        void UpdateJourFerie(APH_CJF_CalendrierJourFerieParameters Parameters);

    }
}
