﻿using Entities.Models;
using Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Contracts
{
    public interface IAPH_RMI_RapprochementManuelleIncohernceRepository : IRepositoryBase<APH_RMI_RapprochementManuelleIncohernce>
    {
        //IEnumerable<APH_V_Monitoring_Partenaire> GetAllPartenaires();
        PagedList<APH_RMI_RapprochementManuelleIncohernce> GetIncoherence(APH_RMI_RapprochementManuelleIncohernceParameters ownerParameters);
        IList<APH_TDA_TranscoDonneesAmplitude> GetCorrectionAutomatique(APH_RMI_RapprochementManuelleIncohernceParameters ownerParameters);
        void UpdateIncoherence(APH_RMI_RapprochementManuelleIncohernceParameters parametres);
        void UpdateIncoherenceManuelle(APH_RMI_RapprochementManuelleIncohernceParameters parametres);
        APH_RMI_RapprochementManuelleIncohernce GetDoublonsById(int idInc);
        //POC_OWN_Owner GetOwnerById(string ownerId);
        //POC_OWN_Owner GetOwnerWithDetails(string ownerId);
        //void CreateOwner(POC_OWN_Owner owner);
        //void UpdateOwner(POC_OWN_Owner owner);
        //void DeleteOwner(POC_OWN_Owner owner);
    }
}
