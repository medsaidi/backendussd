﻿using Entities.Models;
using Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Contracts
{
    public interface IPOC_ACC_AccountRepository : IRepositoryBase<POC_ACC_Account>
    {
        IEnumerable<POC_ACC_Account> AccountsByOwner(string ownerId);
        public PagedList<POC_ACC_Account> GetAccountsByOwner(string ownerId, POC_ACC_AccountParameters parameters);
    }
}
