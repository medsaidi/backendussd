﻿using Entities.Models;
using Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Contracts
{
    public interface IAPH_RMD_RapprochementManuelleDoublonsRepository : IRepositoryBase<APH_RMD_RapprochementManuelleDoublons>
    {
        //IEnumerable<APH_V_Monitoring_Partenaire> GetAllPartenaires();
        PagedList<APH_RMD_RapprochementManuelleDoublons> GetDoublons(APH_RMD_RapprochementManuelleDoublonsParameters Parameters);
        APH_RMD_RapprochementManuelleDoublons GetDoublonsById(int idDoublons);
        bool verificationDoublon(APH_RMD_RapprochementManuelleDoublonsParameters Parameters); 

        void UpdatbyDoubonsObject(APH_RMD_RapprochementManuelleDoublonsParameters Parameters);

        //POC_OWN_Owner GetOwnerById(string ownerId);
        //POC_OWN_Owner GetOwnerWithDetails(string ownerId);
        //void CreateOwner(POC_OWN_Owner owner);
        //void UpdateOwner(POC_OWN_Owner owner);
        //void DeleteOwner(POC_OWN_Owner owner);
    }
}
