﻿using Entities.Models;
using Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Contracts
{
    public interface IAPH_V_Monitoring_PartenaireRepository : IRepositoryBase<APH_V_Monitoring_Partenaire>
    {
        //IEnumerable<APH_V_Monitoring_Partenaire> GetAllPartenaires();
        IList<APH_V_Monitoring_Partenaire> GetMonitoring(APH_V_Monitoring_PartenaireParameters ownerParameters);
       Task EnvoiAmplitude(string source,string statuts);
        IList<APH_V_HSC_HistoriqueSuspendusCorriger> GetSuspendusHis(APH_Extract_Parameters Parameters); 
        IList<APH_V_TRR_TransactionRejeter> GetRejet(APH_Extract_Parameters parameters);
        IList<APH_V_TRT_TransactionTransferer> GetSuccess(APH_Extract_Parameters parameters);
        IList<APH_V_TRC_TransactionReconciliation> GetReconciliation(APH_Extract_Parameters parameters);
        IList<APH_V_RER_RegulationRompus> GetRompus(APH_Extract_Parameters parameters);

        IList<APH_V_EPF_EcheancePartenaireFuture> GetEpf(APH_Extract_Parameters parameters);
        IList<APH_V_TRS_TransactionSuspendus> GetSuspendus(APH_Extract_Parameters parameters);
        //IList<APH_V_TRS_TransactionSuspendus> GetSuspendus(APH_Extract_Parameters parameters);


        //POC_OWN_Owner GetOwnerById(string ownerId);
        //POC_OWN_Owner GetOwnerWithDetails(string ownerId);
        //void CreateOwner(POC_OWN_Owner owner);
        //void UpdateOwner(POC_OWN_Owner owner);
        //void DeleteOwner(POC_OWN_Owner owner);
    }
}
