﻿using Entities.Models;
using Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Contracts
{
    public interface IPOC_OWN_OwnerRepository : IRepositoryBase<POC_OWN_Owner>
    {
        IEnumerable<POC_OWN_Owner> GetAllOwners();
        PagedList<POC_OWN_Owner> GetOwners(POC_OWN_OwnerParameters ownerParameters);
        POC_OWN_Owner GetOwnerById(string ownerId);
        POC_OWN_Owner GetOwnerWithDetails(string ownerId);
        void CreateOwner(POC_OWN_Owner owner);
        void UpdateOwner(POC_OWN_Owner owner);
        void DeleteOwner(POC_OWN_Owner owner);
    }
}
