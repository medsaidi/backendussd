﻿using Entities.Models;
using Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Contracts
{
    public interface IAPH_RDS_RapprochementDesSuspendusRepository : IRepositoryBase<APH_RDS_RapprochementDesSuspendus>
    {
        //IEnumerable<APH_V_Monitoring_Partenaire> GetAllPartenaires();
         
        PagedList<APH_RDS_RapprochementDesSuspendus> GetSuspendus(APH_RDS_RapprochementDesSuspendusParameters Parameters);
        APH_RDS_RapprochementDesSuspendus GetSuspendusById(int IdSuspendus);
        //POC_OWN_Owner GetOwnerWithDetails(string ownerId);
        void CreateSuspendus(APH_RDS_RapprochementDesSuspendus suspendus);
        void UpdateSuspendus(APH_RDS_RapprochementDesSuspendus suspendus);
        void UpdateSuspendusObject(APH_RDS_RapprochementDesSuspendusParameters Parameters);

        bool VerificationCoherence(APH_RDS_RapprochementDesSuspendusParameters Parameters);

        //void DeleteOwner(POC_OWN_Owner owner);
    }
}