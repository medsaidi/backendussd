﻿using Entities.Models;
using Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Contracts
{
    public interface IAPH_HDR_HistoriqueDesRapprochementsRepository : IRepositoryBase<APH_HDR_HistoriqueDesRapprochements>
    {
        //IEnumerable<APH_V_Monitoring_Partenaire> GetAllPartenaires();
        PagedList<APH_HDR_HistoriqueDesRapprochements> GetHistorique(APH_HDR_HistoriqueDesRapprochementsParameters ownerParameters);
        //POC_OWN_Owner GetOwnerById(string ownerId);
        //POC_OWN_Owner GetOwnerWithDetails(string ownerId);
        //void CreateOwner(POC_OWN_Owner owner);
        //void UpdateOwner(POC_OWN_Owner owner);
        //void DeleteOwner(POC_OWN_Owner owner);
    }
}