﻿namespace Contracts
{
    public interface IRepositoryWrapper
    {
        IPOC_OWN_OwnerRepository Owner { get; }
        IPOC_ACC_AccountRepository Account { get; }
        IAPH_V_Monitoring_PartenaireRepository Monitoring { get; }
        IAPH_RMI_RapprochementManuelleIncohernceRepository Incoherences { get; }
        IAPH_RMD_RapprochementManuelleDoublonsRepository Doublons { get; }
        IAPH_HDR_HistoriqueDesRapprochementsRepository Historique { get; }
        IAPH_RDS_RapprochementDesSuspendusRepository Suspendus { get; }
        IAPH_ADT_AnnulataionDesTransacationsRepository Annulation { get; }
        IAPH_VER_VersementEnRouteRepository VersementEnRoutes { get; }
        IAPH_CJF_CalendrierJourFerieRepository CalendrierJourFerie { get; }
        IClientsRepository Clients { get; }
        ITransactionsClientRepository TransactionsClients { get; }
        IClientInfosRepository ClientInfos { get; }

        void Save();
    }
}
