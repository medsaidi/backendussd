﻿using Entities.Models;
using Repository;

namespace Contracts
{
    public interface IClientInfosRepository : IRepositoryBase<ClientInfos>
    {
        PagedList<ClientInfos> GetClientInfos(ClientInfosParameters parameters);

    }
}