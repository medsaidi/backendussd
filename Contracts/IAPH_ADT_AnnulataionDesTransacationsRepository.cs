﻿using Entities.Models;
using Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Contracts
{
    public interface IAPH_ADT_AnnulataionDesTransacationsRepository : IRepositoryBase<APH_ADT_AnnulataionDesTransacations>
    {
        PagedList<APH_ADT_AnnulataionDesTransacations> GetAnnulation(APH_ADT_AnnulataionDesTransacationsParameters ownerParameters);
        APH_ADT_AnnulataionDesTransacations GetAnnulationById(int idAnnulation);
        void UpdateAnnulation(APH_ADT_AnnulataionDesTransacations annulation);

        APH_ADT_AnnulataionDesTransacations UpdateAnnulationObject(APH_ADT_AnnulataionDesTransacationsParameters Parameters);

        public void UpdateArray(APH_ADT_AnnulataionDesTransacationsParameters[] tables);
    }
}
