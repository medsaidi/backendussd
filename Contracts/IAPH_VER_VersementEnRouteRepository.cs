﻿using Entities.Models;
using Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Contracts
{
    public interface IAPH_VER_VersementEnRouteRepository : IRepositoryBase<APH_VER_VersementEnRoute>
    {
        PagedList<APH_VER_VersementEnRoute> GetVersementEnRoutes(APH_VER_VersementEnRouteParameter Parameters);
        APH_VER_VersementEnRoute GetVersementEnRoutesById(int IdVer);
        void CreateVersementEnRoute(APH_VER_VersementEnRoute ver);
        void UpdateVersementEnRoute(APH_VER_VersementEnRoute ver);
        APH_VER_VersementEnRoute UpdateVersementEnRouteObject(APH_VER_VersementEnRouteParameter Parameters);
        public void UpdateArray(APH_VER_VersementEnRouteParameter[] tables);
    }
}