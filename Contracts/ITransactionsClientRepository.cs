﻿using System.IO;
using Entities.Models;
using Repository;

namespace Contracts
{
    public interface ITransactionsClientRepository : IRepositoryBase<TransactionsClient>
    {
        PagedList<TransactionsClient> GetTransactionsClients(TransactionsClientsParameters parameters);
        TransactionsClient[] GetTransactionsClientsToExport(TransactionsClientsParameters parameters);
        MemoryStream MemoryStream(TransactionsClient[] returnedList);
    }
}