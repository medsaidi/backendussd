﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities.Models
{
    [Table("APH_V_RDS_RapprochementDesSuspendus")]
    [Keyless]
    public class APH_RDS_RapprochementDesSuspendus
    {
        public virtual string RDS_Source { get; set; }
        public virtual int RDS_ID { get; set; }
        public virtual string RDS_TransactionId { get; set; }
        public virtual DateTime? RDS_DateTransaction { get; set; }
        public virtual DateTime? RDS_DateInsertion { get; set; }
        public virtual string RDS_CT { get; set; }
        public virtual string RDS_CIN { get; set; }
        public virtual string RDS_Ammount { get; set; }
        public virtual string RDS_CIN_RIM { get; set; }
        public virtual string RDS_CIN_FullName { get; set; }
        public virtual string RDS_CIN_RSM { get; set; }
        public virtual string RDS_CIN_CIN { get; set; }
        public virtual string RDS_CIN_CT { get; set; }
        public virtual DateTime? RDS_CIN_DateEcheance { get; set; }
        public virtual string RDS_CIN_ResteCredit { get; set; }
        public virtual string RDS_CT_RIM { get; set; }
        public virtual string RDS_CT_FullName { get; set; }
        public virtual string RDS_CT_RSM { get; set; }
        public virtual string RDS_CT_CIN { get; set; }
        public virtual string RDS_CT_CT { get; set; }
        public virtual DateTime? RDS_CT_DateEcheance { get; set; }
        public virtual string RDS_CT_ResteCredit { get; set; }
    }
}
