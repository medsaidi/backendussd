﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities.Models
{
    
    public class APH_CJF_CalendrierJourFerieParameters
    {
        public virtual int CJF_ID { get; set;  }

        public virtual DateTime? CJF_DateFerie { get; set;  }

        public virtual string CJF_Description { get; set;  }

        public Boolean CJF_Status { get; set;  }

    }
}
