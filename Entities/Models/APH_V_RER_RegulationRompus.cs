﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities.Models
{
    [Table("APH_V_RER_RegulationRompus")]
    [Keyless]
    public class APH_V_RER_RegulationRompus
    {
        public virtual string RER_CIN { get; set; }
        public virtual string RER_CT { get; set; }
        public virtual string RER_MontantVerser { get; set; }
        public virtual string RER_FullName { get; set; }
        public virtual DateTime RER_DueDate { get; set; }
        public virtual string RER_Description { get; set; }
        public virtual string RER_Motif { get; set; }
        public virtual DateTime? RER_Creartion { get; set; }
        public virtual DateTime? RER_DateTransaction { get; set; }
    }
}
