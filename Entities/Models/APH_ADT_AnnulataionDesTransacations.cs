﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
namespace Entities.Models
{
    [Table("APH_V_ADT_AnnulataionDesTransacations")]
    [Keyless]
    public class APH_ADT_AnnulataionDesTransacations
    {

        public int ADT_ID { get; set; }
        public DateTime ADT_DateOperation { get; set; }
        public DateTime ADT_DateTransaction { get; set; }
        public string ADT_CodePartenaire { get; set; }
        public string ADT_CT { get; set; }
        public string ADT_CIN { get; set; }
        public string ADT_MontantVerse { get; set; }
        //public string ADT_NomBackOffice { get; set; }
        public string ADT_Libelle { get; set; }
        //public string ADT_Decision { get; set; }


    }
}

