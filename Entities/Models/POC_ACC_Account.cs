﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities.Models
{
    [Table("POC_ACC_Account")]
    public class POC_ACC_Account
    {
        [Key]
        [Column("ACC_AccountId")]
        public string Id { get; set; }

        [Required(ErrorMessage = "Date created is required")]
        public DateTime ACC_DateCreated { get; set; }

        [Required(ErrorMessage = "Account type is required")]
        public string ACC_AccountType { get; set; }

        [ForeignKey(nameof(POC_OWN_Owner))]
        public string ACC_OwnerID { get; set; }
        public POC_OWN_Owner POC_OWN_Owner { get; set; }
    }
}
