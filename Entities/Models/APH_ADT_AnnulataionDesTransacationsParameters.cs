﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
 
using Repository;
namespace Entities.Models
{
   public class APH_ADT_AnnulataionDesTransacationsParameters : QueryStringParameters
    {

        public APH_ADT_AnnulataionDesTransacationsParameters()
        {
            OrderBy = "Partenaire";
        }
        public DateTime DateOp { get; set; }
        public string Partenaire { get; set; }
        public int ADT_ID { get; set; }
        public string ADT_CompteTechnique { get; set; }
        public string ADT_CIN { get; set; }
        public string ADT_Decision { get; set; }
        public string ADT_MontantVerse { get; set; }
        public string ADT_Commentaire { get; set; }
        public DateTime? ADT_DateOperation { get; set; }
        public string ADT_UserModified { get; set; }
    }

    
}
