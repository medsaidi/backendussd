﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities.Models
{
    [Table("POC_OWN_Owner")]
    public class POC_OWN_Owner
    {
        [Key]
        [Column("OWN_OwnerID")]
        public string ID { get; set; }

        [Required(ErrorMessage = "Name is required")]
        [StringLength(60, ErrorMessage = "Name can't be longer than 60 characters")]
        public string OWN_Name { get; set; }

        [Required(ErrorMessage = "Date of birth is required")]
        public DateTime OWN_DateOfBirth { get; set; }

        [Required(ErrorMessage = "Address is required")]
        [StringLength(100, ErrorMessage = "Address cannot be loner then 100 characters")]
        [Column("OWN_Adress")]
        public string OWN_Address { get; set; }

        public ICollection<POC_ACC_Account> OWN_Accounts { get; set; }
    }
}
