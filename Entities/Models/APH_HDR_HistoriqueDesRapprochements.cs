﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities.Models
{
    [Table("APH_HDR_HistoriqueDesRapprochements")]
    public class APH_HDR_HistoriqueDesRapprochements
    {
        [Key]
        public virtual int HDR_ID { get; set; }
        public virtual DateTime? HDR_DateTransaction { get; set; }
        public virtual string HDR_CodePartenaire { get; set; }
        public virtual string HDR_CT { get; set; }
        public virtual string HDR_CTRectifie { get; set; }
        public virtual string HDR_CIN { get; set; }
        public virtual string HDR_CINRectifie { get; set; }
        public virtual string HDR_OldStatus { get; set; }
        public virtual string HDR_NewStatus { get; set; }
        public virtual string HDR_MontantVerse { get; set; }
        public virtual DateTime? HDR_DateRectification { get; set; }
        public virtual string HDR_NomBackOffice { get; set; }
        public virtual string HDR_Commentaire { get; set; }
        public virtual string HDR_RSM { get; set; }
        public virtual DateTime? HDR_CreatedOn { get; set; }
        public virtual string HDR_CreatedBy { get; set; }
        public virtual DateTime? HDR_UpdatedOn { get; set; }
        public virtual string HDR_UpdatedBy { get; set; }
    }
}