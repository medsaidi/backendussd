﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entities.Models
{
    [Table("ClientInfos")]
    public class ClientInfos
    {
        [Key] public virtual int Id { get; set; }
        public virtual string Mobile { get; set; }
        public virtual string AccountLabel { get; set; }
        public virtual string AccountNumber { get; set; }
        public virtual DateTime DateCreated { get; set; }
        public virtual string Branch { get; set; }
        public virtual bool Action { get; set; }
        public virtual bool? Status { get; set; }
        public virtual string Alias { get; set; }
        public virtual string AmplitudeId { get; set; }
        
        [ForeignKey("Client")]
        public int IdClient { get; set; }
        public virtual Client Client { get; set; }
    }
}