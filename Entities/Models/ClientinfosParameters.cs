﻿using Repository;

namespace Entities.Models
{
    public class ClientInfosParameters : QueryStringParameters
    {
        public int IdClient { get; set; }
    }
}