﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities.Models
{
    [Table("attijeri_transaction_payee")]
    public class attijeri_transaction_payee
    {
        [Key]
        public virtual int id { get; set; }
        public virtual string cin { get; set; }
        public virtual string technical_account { get; set; }
        public virtual string agence { get; set; }
        public virtual string amount_monthly_installment { get; set; }
        public virtual DateTime? date_transaction { get; set; }
        public virtual string no_transaction { get; set; }
        public virtual string source { get; set; }
        public virtual string description { get; set; }
        public virtual DateTime? date_insertion { get; set; }
        public virtual string status_transaction { get; set; }
        public virtual string bank_transaction_id { get; set; }
    }
}
