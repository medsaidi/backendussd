﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities.Models
{
    [Table("APH_V_TRC_TransactionReconciliation")]
    [Keyless]
    public class APH_V_TRC_TransactionReconciliation
    {
        public virtual string TRC_Partenaire { get; set; }
        public virtual string TRC_CIN { get; set; }
        public virtual string TRC_CT { get; set; }
        public virtual string TRC_Amount { get; set; }
        public virtual DateTime? TRC_DueDate { get; set; }
        public virtual DateTime? TRC_DateTransaction { get; set; }
        public virtual DateTime? TRC_DateInsertion { get; set; }
        public virtual DateTime? TRC_DateOperation { get; set; }
        public virtual string TRC_RestAmount { get; set; }
        public virtual string TRC_StatutTransaction { get; set; }
        public virtual string TRC_FullName { get; set; }
    }
}
