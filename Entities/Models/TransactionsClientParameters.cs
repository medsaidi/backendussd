﻿using System;
using Repository;

namespace Entities.Models
{
    public class TransactionsClientsParameters : QueryStringParameters
    {
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
    }
}