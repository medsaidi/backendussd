﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities.Models
{
    [Table("APH_V_TRT_TransactionTransferer")]
    [Keyless]
    public class APH_V_TRT_TransactionTransferer
    {
        public virtual string TRT_Partenaire { get; set; }
        public virtual string TRT_Agence { get; set; }
        public virtual string TRT_CIN { get; set; }
        public virtual string TRT_CT { get; set; }
        public virtual string TRT_Amount { get; set; }
        public virtual DateTime? TRT_DueDate { get; set; }
        public virtual DateTime? TRT_DateInsertion { get; set; }
        public virtual DateTime? TRT_DateTransaction { get; set; }
        public virtual string TRT_RestAmount { get; set; }
        public virtual string TRT_FullName { get; set; }
    }
}
