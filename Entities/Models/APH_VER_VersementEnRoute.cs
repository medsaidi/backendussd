﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities.Models
{
    [Table("APH_V_VER_VersementEnRoute")]
    public class APH_VER_VersementEnRoute
    {
        [Key]
        public virtual int VER_ID { get; set; }
        public virtual string VER_Source { get; set; }
        public virtual DateTime? VER_DateTransaction { get; set; }
        public virtual string VER_Libelle { get; set; }
        public virtual string VER_Credit { get; set; }
        public virtual string VER_Debit { get; set; }
    }
}
