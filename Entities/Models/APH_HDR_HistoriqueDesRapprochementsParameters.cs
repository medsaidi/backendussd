﻿using Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities.Models
{
    public class APH_HDR_HistoriqueDesRapprochementsParameters : QueryStringParameters
    {
        public APH_HDR_HistoriqueDesRapprochementsParameters()
        {
            OrderBy = "name";
        }
   

        public DateTime? HDR_DateOperation { get; set; }
        public DateTime? HDR_DateOperation_Fin { get; set; }
        public string HDR_CodePartenaire { get; set; }
    }
}