﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entities.Models
{
    [Table("Client")]
    public class Client
    {
        [Key] public virtual int Id { get; set; }
        public virtual DateTime? DateCreated { get; set; }
        public virtual string FirstName { get; set; }
        public virtual string LastName { get; set; }
        public virtual string PhoneNumber { get; set; }
        public virtual string Branch { get; set; }
        public virtual bool? Ussd { get; set; }
        public virtual string MobileMoney { get; set; }
        public virtual string AmplitudeId { get; set; }
        public string PIN { get; set; }
    }
}