﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities.Models
{
    [Table("APH_V_RMI_RapprochementManuelleIncohernce")]
    [Keyless]
    public class APH_RMI_RapprochementManuelleIncohernce
    {
        public virtual string RMI_Source { get; set; }
        public virtual int RMI_ID { get; set; }
        public virtual string RMI_TransactionId { get; set; }
        public virtual DateTime? RMI_DateTransaction { get; set; }
        public virtual DateTime? RMI_DateInsertion { get; set; }
        public virtual string RMI_CT { get; set; }
        public virtual string RMI_CIN { get; set; }
        public virtual string RMI_Ammount { get; set; }
        public virtual string RMI_CIN_RIM { get; set; }
        public virtual string RMI_CIN_CIN { get; set; }
        public virtual string RMI_CIN_CT { get; set; }
        public virtual string RMI_CIN_FullName { get; set; }
        public virtual DateTime? RMI_CIN_DateEcheance { get; set; }
        public virtual string RMI_CIN_ResteAPayer { get; set; }
        public virtual string RMI_CIN_RSM { get; set; }
        public virtual string RMI_CT_RIM { get; set; }
        public virtual string RMI_CT_CIN { get; set; }
        public virtual string RMI_CT_CT { get; set; }
        public virtual string RMI_CT_FullName { get; set; }
        public virtual DateTime? RMI_CT_DateEcheance { get; set; }
        public virtual string RMI_CT_ResteAPayer { get; set; }
        public virtual string RMI_CT_RSM { get; set; }
        public virtual string RMI_ISC_Raison { get; set; }
        public virtual string RMI_ISC_ToDo { get; set; }
        public virtual string RMI_ISC_Percentage { get; set; }
        public virtual string RMI_ISC_CtSuggested { get; set; }
        public virtual string RMI_ISC_CinSuggested { get; set; }
        public virtual string RMI_ISC_MontantSuggested { get; set; }
        public virtual string RMI_ISC_CustemerIdSuggested { get; set; }
        public virtual string RMI_ISC_FullNameCustumerSuggested { get; set; }
        public virtual DateTime? RMI_ISC_DAteEcheanceSuggested { get; set; }

        [NotMapped]
        public string ConcatCtCin {
            get 
            {
                return this.RMI_CIN + this.RMI_CT;
            }
            set
            {
            }
         }
        public APH_RMI_RapprochementManuelleIncohernce()
        {
        }
    }
}
