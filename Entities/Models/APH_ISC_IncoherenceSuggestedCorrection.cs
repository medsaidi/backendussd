﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities.Models
{
    [Table("APH_ISC_IncoherenceSuggestedCorrection")]
    public class APH_ISC_IncoherenceSuggestedCorrection
    {
        [Key]
        public virtual int ISC_Id { get; set; }
        public virtual string ISC_TransactionId { get; set; }
        public virtual string ISC_Raison { get; set; }
        public virtual string ISC_ToDo { get; set; }
        public virtual string ISC_Percenatge { get; set; }
        public virtual string ISC_CtSuggested { get; set; }
        public virtual string ISC_CinSuggested { get; set; }
        public virtual DateTime? ISC_CreatedOn { get; set; }
        public virtual string ISC_CreatedBy { get; set; }
        public virtual DateTime? ISC_UpdatedOn { get; set; }
        public virtual string ISC_UpdatedBy { get; set; }
    }
}
