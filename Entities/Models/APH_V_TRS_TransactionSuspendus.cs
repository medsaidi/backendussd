﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities.Models
{
    [Table("APH_V_TRS_TransactionSuspendus")]
    [Keyless]
    public class APH_V_TRS_TransactionSuspendus
    {
        public virtual DateTime? TRS_DateDiffusion { get; set; }
        public virtual DateTime? TRS_DateTransaction { get; set; }
        public virtual string TRS_Partenaire { get; set; }
        public virtual string TRS_Montant { get; set; }
        public virtual string TRS_Agence { get; set; }
        public virtual string TRS_LIBELLE { get; set; }
    }
}
