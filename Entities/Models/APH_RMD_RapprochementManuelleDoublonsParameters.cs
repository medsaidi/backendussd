﻿using Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities.Models
{
    public class APH_RMD_RapprochementManuelleDoublonsParameters : QueryStringParameters
    {
        public APH_RMD_RapprochementManuelleDoublonsParameters()
        {
            OrderBy = "name";
        }
        public virtual int RMD_ID { get; set; }
        public virtual string RMD_Source { get; set; }
        public virtual DateTime? RMD_DateTransaction { get; set; }
        public virtual string RMD_O_DateTransaction { get; set; }
        public virtual string RMD_O_Decision { get; set; }
        public virtual int RMD_ID_Doublons { get; set; }
        public virtual string RMD_D_Decision { get; set; }
        public virtual string RMD_UserModified { get; set; }
        public virtual string RMD_O_Commentaire { get; set; }
        public virtual string RMD_D_Commentaire { get; set; }

    }


}
