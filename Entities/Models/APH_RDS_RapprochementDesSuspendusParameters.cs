﻿using Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities.Models
{
    public class APH_RDS_RapprochementDesSuspendusParameters : QueryStringParameters
    {
        public APH_RDS_RapprochementDesSuspendusParameters()
        {
            OrderBy = "Partenaire";
        }
        //public uint MinYearOfBirth { get; set; }
        //public uint MaxYearOfBirth { get; set; } = (uint)DateTime.Now.Year;
        //public bool ValidYearRange => MaxYearOfBirth > MinYearOfBirth;
        //public string Name { get; set; }
        public virtual string RDS_Source { get; set; }
        public virtual int RDS_ID { get; set; }
        public virtual string RDS_UserModified { get; set; }
        public virtual DateTime? RDS_DateTransaction { get; set; }
        public virtual string RDS_CT { get; set; }
        public virtual string RDS_CIN { get; set; }
        public virtual string RDS_Decision { get; set; }
        public virtual string RDS_Commentaire { get; set; }
    }
}