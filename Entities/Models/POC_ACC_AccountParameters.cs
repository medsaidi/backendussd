﻿using Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities.Models
{
    public class POC_ACC_AccountParameters : QueryStringParameters
    {
        public POC_ACC_AccountParameters()
        {
            OrderBy = "DateCreated";
        }
    }
}
