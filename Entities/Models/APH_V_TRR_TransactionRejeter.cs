﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities.Models
{
    [Table("APH_V_TRR_TransactionRejeter")]
    [Keyless]
    public class APH_V_TRR_TransactionRejeter
    {
        public virtual string TRR_GSM { get; set; }
        public virtual string TRR_DueAmount { get; set; }
        public virtual string TRR_RestAmount { get; set; }
        public virtual DateTime? TRR_DueDate { get; set; }
        public virtual DateTime? TRR_DateInsertion { get; set; }
        public virtual string TRR_CT { get; set; }
        public virtual string TRR_CIN { get; set; }
        public virtual string TRR_RIM { get; set; }
        public virtual string TRR_Agency { get; set; }
        public virtual string TRR_Comment { get; set; }
        public virtual string TRR_Partner { get; set; }
    }
}
