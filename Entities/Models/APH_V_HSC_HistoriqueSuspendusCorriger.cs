﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities.Models
{
    [Table("APH_V_HSC_HistoriqueSuspendusCorriger")]
    [Keyless]
   public class APH_V_HSC_HistoriqueSuspendusCorriger
    {
        public virtual string HSC_CodePArtenaire { get; set;  }
        public virtual DateTime? HSC_DateTransaction { get; set;  }

        public virtual string HSC_CIN { get; set;  }

        public virtual string HSC_CINRectifie { get; set; }

        public virtual string HSC_Ct { get; set; }
        public virtual string HSC_CtRectifie { get; set; }

        public virtual string HSC_Montant { get; set; }

        public virtual DateTime? HSC_DateRectification { get; set; }
        public virtual string HSC_StatutTransaction { get; set; }

        public virtual string HSC_NomBackOffice { get; set; }

        public virtual DateTime? HSC_DateOperation { get; set; }
        public virtual DateTime? HSC_DateInsertion { get; set; }

        public virtual DateTime? HSC_DateSuspension { get; set; }
        public virtual string HSC_NomSuspension { get; set; }

     



    }
}
