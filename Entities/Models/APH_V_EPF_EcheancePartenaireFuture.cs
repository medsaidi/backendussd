﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities.Models
{
    [Table("APH_V_EPF_EcheancePartenaireFuture")]
    [Keyless]
    public class APH_V_EPF_EcheancePartenaireFuture
    {
        public virtual string EPF_Partenaire { get; set; }
        public virtual string EPF_ClientId { get; set; }
        public virtual string EPF_FullName { get; set; }
        public virtual string EPF_CIN { get; set; }
        public virtual string EPF_CT { get; set; }
        public virtual DateTime? EPF_DateEchance { get; set; }
        public virtual string EPF_MontantToPay { get; set; }
        public virtual string EPF_MontantEcheance { get; set; }


    }
}
