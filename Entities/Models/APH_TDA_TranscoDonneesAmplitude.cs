﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities.Models
{
    [Table("ref_information_client")]
    [Keyless]
    public class APH_TDA_TranscoDonneesAmplitude
    {
        public virtual string TECHNICAL_ACCOUNT_AMPLITUDE { get; set; }
        public virtual string CUSTOMERID { get; set; }
        public virtual string CIN { get; set; }
        public virtual string TECHNICAL_ACCOUNT_ORBIT { get; set; }
        public virtual string RIB { get; set; }
        public virtual string RIP { get; set; }
        //public virtual string AUTO_DEBIT_CARD { get; set; }
        public virtual string NUM_CARD { get; set; }
        public virtual string NUM_DOMICILIATION { get; set; }
        public virtual string REIMBURSEMENT_METHOD { get; set; }
        public virtual string BANK_NAME { get; set; }
        public virtual string ACCOUNT_CLOSE { get; set; }
        public virtual string ACCOUNT_INST_CLOSE { get; set; }
        public virtual string AMOUNT_MONTHLY_INSTALLEMENT { get; set; }
        public virtual DateTime? DATE_MONTHLY_INSTALLEMENT { get; set; }

    }
}