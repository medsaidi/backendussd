﻿using Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities.Models
{
    public class APH_VER_VersementEnRouteParameter : QueryStringParameters
    {
        public APH_VER_VersementEnRouteParameter()
        {
            OrderBy = "VER_Partenaire";
        }
        public int VER_ID { get; set; }
        public string VER_Partenaire { get; set; }
        public string VER_NumeroCompte { get; set; }
        public DateTime? VER_DateOperation { get; set; }
        public string VER_Libelle { get; set; }
        public string VER_Decision { get; set; }
        public  string VER_UserModified { get; set; }
        public string VER_Debit { get; set; }
        public string VER_Credit { get; set; }
   

    }
}