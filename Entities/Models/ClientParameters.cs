﻿using Repository;

namespace Entities.Models
{
    public class ClientParameters : QueryStringParameters
    {
        public string AmplitudeId { get; set; }
        public string Branch { get; set; }
        public string Phone { get; set; }
    }
}