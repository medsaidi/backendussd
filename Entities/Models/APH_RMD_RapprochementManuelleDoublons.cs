﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities.Models
{
    [Table("APH_V_RMD_RapprochementManuelleDoublons")]
    [Keyless]
    public class APH_RMD_RapprochementManuelleDoublons
    {
        public virtual string RMD_Source { get; set; }
        public virtual int RMD_ID { get; set; }
        
        public virtual DateTime? RMD_DateTransaction { get; set; }
        public virtual string RMD_CT { get; set; }
        public virtual string RMD_CIN { get; set; }
        public virtual string RMD_FullName { get; set; }
        public virtual string RMD_RIM { get; set; }
        public virtual string RMD_RSM { get; set; }
        public virtual string RMD_O_DateTransaction { get; set; }
        public virtual string RMD_O_MontantVerse { get; set; }
        public virtual string RMD_O_Agence { get; set; }
        public virtual string RMD_O_Decision { get; set; }
        public virtual int RMD_ID_Doublons { get; set; }
        public virtual string RMD_D_DateTransaction { get; set; }
        public virtual string RMD_D_MontantVerse { get; set; }
        public virtual string RMD_D_Agence { get; set; }
        public virtual string RMD_D_Decision { get; set; }
    }
}
