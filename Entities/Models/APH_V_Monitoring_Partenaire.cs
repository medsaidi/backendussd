﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities.Models
{
    [Table("APH_V_MOP_MonitoringOnPartenaire")]
    [Keyless]
    public class APH_V_Monitoring_Partenaire
    {
        public string MOP_DateOperation { get; set; }
        public string MOP_Source { get; set; }
        public int MOP_Total_Lignes { get; set; }
        public int MOP_NEW { get; set; }
        public int MOP_CORRECTED { get; set; }
        public int MOP_SUCCESS { get; set; }
        public int MOP_INCOHERENCE { get; set; }
        public int MOP_DOUBLE { get; set; }
        public int MOP_SUSPENDED { get; set; }
        public int MOP_CASH_PAYMENT { get; set; }
        public int MOP_ANNULATION { get; set; }
        public int MOP_REJECTED { get; set; }
        public int MOP_AmplitudeError { get; set; }
    }
}
