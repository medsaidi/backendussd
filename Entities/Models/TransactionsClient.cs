﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entities.Models
{
    [Table("TransactionsClient")]
    public class TransactionsClient
    {
        [Key] public virtual int BankId { get; set; }
        public virtual int TelcoId { get; set; }
        public virtual decimal? Amount { get; set; }
        public virtual int? Fee { get; set; }
        public virtual bool? Status { get; set; }
        public virtual string Reason { get; set; }
        public virtual string TransactionType { get; set; }
        public virtual string? ClientName { get; set; }
        public virtual string DestinationAccountNumber { get; set; }
        public virtual string OriginAccountNumber { get; set; }
        public virtual string Branch { get; set; }
        public virtual DateTime? Date { get; set; }
        public virtual string Network { get; set; }
    }
}