﻿using Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities.Models
{
    public class APH_RMI_RapprochementManuelleIncohernceParameters : QueryStringParameters
    {
        public APH_RMI_RapprochementManuelleIncohernceParameters()
        {
            OrderBy = "name";
        }
        //public uint MinYearOfBirth { get; set; }
        //public uint MaxYearOfBirth { get; set; } = (uint)DateTime.Now.Year;
        //public bool ValidYearRange => MaxYearOfBirth > MinYearOfBirth;
        //public string Name { get; set; }
        public virtual string RMI_Source { get; set; }
        public virtual string RMI_UserModified { get; set; }
        public virtual int RMI_ID { get; set; }
        public virtual string RMI_TransactionId { get; set; }
        public virtual DateTime? RMI_DateTransaction { get; set; }
        public virtual DateTime? RMI_DateInsertion { get; set; }
        public virtual string RMI_CT { get; set; }
        public virtual string RMI_CIN { get; set; }
        public virtual string RMI_Ammount { get; set; }
        public virtual string RMI_CIN_RIM { get; set; }
        public virtual string RMI_CIN_CIN { get; set; }
        public virtual string RMI_CIN_CT { get; set; }
        public virtual string RMI_CT_RIM { get; set; }
        public virtual string RMI_CT_CIN { get; set; }
        public virtual string RMI_CT_CT { get; set; }
        public virtual string RMI_ISC_Raison { get; set; }
        public virtual string RMI_ISC_ToDo { get; set; }
        public virtual string Decision_Manuelle { get; set; }
        public virtual string RMI_ISC_Percentage { get; set; }
        public virtual string RMI_ISC_CtSuggested { get; set; }
        public virtual string RMI_ISC_CinSuggested { get; set; }
        public virtual string RMI_Commentaire { get; set; }
    }
}
