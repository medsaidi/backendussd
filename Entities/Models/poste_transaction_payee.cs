﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities.Models
{
    [Table("poste_transaction_payee")]
    public class poste_transaction_payee
    {
        
        public virtual DateTime DateTransaction { get; set; }
        public virtual string NoTransaction { get; set; }
        [Key]
        public virtual int Id { get; set; }
        public virtual string Cin { get; set; }
        public virtual string TechnicalAccount { get; set; }
        public virtual string LoanAccount { get; set; }
        public virtual string AmountMonthlyInstallment { get; set; }
        public virtual DateTime? DateMonthlyInstallment { get; set; }
        public virtual string Source { get; set; }
        public virtual string Reference { get; set; }
        public virtual string ResteCredit { get; set; }
        public virtual string AutoDebitCard { get; set; }
        public virtual string Gsm { get; set; }
        public virtual DateTime? DateInsertion { get; set; }
        public virtual string StatusTransaction { get; set; }
        public virtual string BankTransactionId { get; set; }
        public virtual string FileSource { get; set; }
    }
}
