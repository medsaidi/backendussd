﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.DataTransferObjects
{
    public class POC_ACC_AccountDTO
    {
        public string Id { get; set; }
        public DateTime ACC_DateCreated { get; set; }
        public string ACC_AccountType { get; set; }
    }
}
