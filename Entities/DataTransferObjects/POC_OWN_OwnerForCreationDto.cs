﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Entities.DataTransferObjects
{
    public class POC_OWN_OwnerForCreationDto
    {
        [Required(ErrorMessage = "Id is required")]
        [StringLength(60, ErrorMessage = "Id must be equal to 36")]
        public string Id { get; set; }
        [Required(ErrorMessage = "Name is required")]
        [StringLength(60, ErrorMessage = "Name can't be longer than 60 characters")]
        public string OWN_Name { get; set; }
        [Required(ErrorMessage = "Date of birth is required")]
        public DateTime OWN_DateOfBirth { get; set; }
        [Required(ErrorMessage = "Address is required")]
        [StringLength(100, ErrorMessage = "Address cannot be loner then 100 characters")]
        public string OWN_Address { get; set; }

    }
}
