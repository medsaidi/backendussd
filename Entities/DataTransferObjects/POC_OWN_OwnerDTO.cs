﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.DataTransferObjects
{
    public class POC_OWN_OwnerDTO
    {
        public string Id { get; set; }
        public string OWN_Name { get; set; }
        public DateTime OWN_DateOfBirth { get; set; }
        public string OWN_Address { get; set; }
        public IEnumerable<POC_ACC_AccountDTO> OWN_Accounts { get; set; }
                                               
    }
}
