﻿using Entities.Authentication;
using Entities.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Entities
{
    public class RepositoryContext : IdentityDbContext<ApplicationUser>
    {
        public RepositoryContext(DbContextOptions options)
            : base(options)
        {
        }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<ApplicationUser>().ToTable("UBO_USR_Users"); //AspNetUsers
            builder.Entity<IdentityRole>().ToTable("UBO_RLE_Roles"); //AspNetRoles
            builder.Entity<IdentityUserRole<string>>().ToTable("UBO_USR_UserRole"); //AspNetUserRole
            builder.Entity<IdentityUserClaim<string>>().ToTable("UBO_USC_UserClaim"); //AspNetUserClaim
            builder.Entity<IdentityUserLogin<string>>().ToTable("UBO_USL_UserLogin"); //AspNetUserLogin
            builder.Entity<IdentityRoleClaim<string>>().ToTable("UBO_RLC_RoleClaim"); //AspNetRoleClaim
            builder.Entity<IdentityUserToken<string>>().ToTable("UBO_UST_UserToken");
        }
        public DbSet<POC_OWN_Owner> Owners { get; set; }
        public DbSet<POC_ACC_Account> Accounts { get; set; }
        public DbSet<ApplicationUser> User { get; set; }
        public DbSet<APH_V_Monitoring_Partenaire> Monitoring { get; set; }
        public DbSet<APH_RMI_RapprochementManuelleIncohernce> Incoherences { get; set; }
        public DbSet<APH_RMD_RapprochementManuelleDoublons> Doublons { get; set; }
        public DbSet<APH_RDS_RapprochementDesSuspendus> Suspendus { get; set; }
        public DbSet<APH_HDR_HistoriqueDesRapprochements> Historique { get; set; }
        public DbSet<APH_ADT_AnnulataionDesTransacations> Annulation { get; set; }
        public DbSet<APH_TDA_TranscoDonneesAmplitude> TranscoAmplitude { get; set; }
        public DbSet<APH_ISC_IncoherenceSuggestedCorrection> Corrections { get; set; }
        public DbSet<amen_transaction_payee> TransactionAmen { get; set; }
        public DbSet<attijeri_transaction_payee> TransactionAWB { get; set; }
        public DbSet<poste_transaction_payee> TransactionPoste { get; set; }
        public DbSet<viamobile_transaction_payee> TransactionViaMobile { get; set; }
        public DbSet<APH_VER_VersementEnRoute> VersementEnRoutes { get; set; }
        public DbSet<APH_CJF_CalendrierJourFerie> CalendrierJourFeries { get; set; }
        public DbSet<APH_V_TRT_TransactionTransferer> TransactionTransferer { get; set; }
        public DbSet<APH_V_TRR_TransactionRejeter> TransactionRejeter { get; set; }
        public DbSet<APH_V_TRC_TransactionReconciliation> TransactionReconciliation { get; set; }
        public DbSet<APH_V_RER_RegulationRompus> RegulationRompus { get; set; }
        public DbSet<APH_V_TRS_TransactionSuspendus> TransactionSuspendus { get; set; }
        public DbSet<APH_V_EPF_EcheancePartenaireFuture> EcheancePartenaireFuture { get; set; }

        public DbSet<APH_V_HSC_HistoriqueSuspendusCorriger> HistoriqueSuspendusCorriger { get; set; }
        public DbSet<Client> Clients { get; set; } 
        public DbSet<TransactionsClient> TransactionsClients { get; set; }
        public DbSet<ClientInfos> ClientsInfos { get; set; }
    }
}
