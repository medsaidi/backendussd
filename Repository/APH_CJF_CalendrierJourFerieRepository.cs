﻿using Contracts;
using Entities;
using Entities.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Repository
{
   public  class APH_CJF_CalendrierJourFerieRepository : RepositoryBase<APH_CJF_CalendrierJourFerie>, IAPH_CJF_CalendrierJourFerieRepository
    {
        private ISortHelper<APH_CJF_CalendrierJourFerie> _sortHelper;
        private RepositoryContext _repositoryContext;
        public APH_CJF_CalendrierJourFerieRepository(RepositoryContext repositoryContext, ISortHelper<APH_CJF_CalendrierJourFerie> sortHelper)
            : base(repositoryContext)
        {
            _sortHelper = sortHelper;
            _repositoryContext = repositoryContext;

        }



        public PagedList<APH_CJF_CalendrierJourFerie> GetJourFeries(APH_CJF_CalendrierJourFerieParameters Parameters)
        {
            var returnedList = FindByCondition(
                  o =>
                      "true" == "true"
              );

            //SearchByName(ref returnedList, Parameters);

            //var filter = new Expression<Func<APH_RDS_RapprochementDesSuspendus, bool>>();

            var _incoherenceItems =
                _repositoryContext.Set<APH_CJF_CalendrierJourFerie>()
                    .Where(
                        x =>
                            x.CJF_DateFerie.Equals(Parameters.CJF_DateFerie)
                            || Parameters.CJF_DateFerie == null
                     )
                    .AsNoTracking()
                    .AsQueryable();
            return PagedList<APH_CJF_CalendrierJourFerie>
                    .ToPagedList(_incoherenceItems, 1, 500);
        }

        public void CreateJourFerie(APH_CJF_CalendrierJourFerieParameters parameters)
        {
            APH_CJF_CalendrierJourFerie Jours = new APH_CJF_CalendrierJourFerie();
            Jours.CJF_Description = parameters.CJF_Description;
            Jours.CJF_DateFerie = parameters.CJF_DateFerie;
            Jours.CJF_Status = true;
            Create(Jours);
        }



        public APH_CJF_CalendrierJourFerie GetJourFeriesById(int IdJours)
        {
            return FindByCondition(sus => sus.CJF_ID.Equals(IdJours))
              .FirstOrDefault();
        }

        public void UpdateJourFerie(APH_CJF_CalendrierJourFerieParameters Parameters)
        {
            var Jour = GetJourFeriesById(Parameters.CJF_ID);
            Jour.CJF_Status = false;
            Update(Jour);
        }
    }
}
