﻿using Contracts;
using Entities;
using Entities.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Linq.Dynamic.Core;
namespace Repository
{
    public class APH_RMD_RapprochementManuelleDoublonsRepository : RepositoryBase<APH_RMD_RapprochementManuelleDoublons>, IAPH_RMD_RapprochementManuelleDoublonsRepository
    {
        private ISortHelper<APH_RMD_RapprochementManuelleDoublons> _sortHelper;
        private RepositoryContext _repositoryContext;
        public APH_RMD_RapprochementManuelleDoublonsRepository(RepositoryContext repositoryContext, ISortHelper<APH_RMD_RapprochementManuelleDoublons> sortHelper)
            : base(repositoryContext)
        {
            _sortHelper = sortHelper;
            _repositoryContext = repositoryContext;
        }

        public PagedList<APH_RMD_RapprochementManuelleDoublons> GetDoublons(APH_RMD_RapprochementManuelleDoublonsParameters Parameters)
        {
            var returnedList = FindByCondition(
                o =>
                    "true" == "true"
            );
            //SearchByName(ref returnedList, Parameters.Name);
            //var sortedOwners = _sortHelper.ApplySort(owners, ownerParameters.OrderBy);
            var _incoherenceItems =
                _repositoryContext.Set<APH_RMD_RapprochementManuelleDoublons>()
                    .Where(
                        x =>
                            x.RMD_Source.ToLower()
                                .Contains(Parameters.RMD_Source)
                            || string.IsNullOrEmpty(Parameters.RMD_Source)
                     )
                    .Where(
                        x =>
                            (Parameters.RMD_DateTransaction != null ?
                                ( x.RMD_DateTransaction.Value.Year== Parameters.RMD_DateTransaction.Value.Year) &&
                                (x.RMD_DateTransaction.Value.Month == Parameters.RMD_DateTransaction.Value.Month) &&
                                (x.RMD_DateTransaction.Value.Day == Parameters.RMD_DateTransaction.Value.Day)
                                : true
                            )
                            
                     )
                    .AsNoTracking()
                    .AsQueryable()
                    .OrderByDescending(x => x.RMD_DateTransaction);
            return PagedList<APH_RMD_RapprochementManuelleDoublons>
                    .ToPagedList(_incoherenceItems, 1,500);
        }
        public APH_RMD_RapprochementManuelleDoublons GetDoublonsById(int idDoublons)
        {
           
            return FindByCondition(dou => dou.RMD_ID.Equals(idDoublons))
                 .FirstOrDefault();

        }

        public APH_RMD_RapprochementManuelleDoublons GetDoublonsByIdDoublon(int idDoublons)
        {

            return FindByCondition(dou => dou.RMD_ID.Equals(idDoublons))
                 .FirstOrDefault();

        }
        private void SearchByName(ref IQueryable<APH_V_Monitoring_Partenaire> owners, string param)
        {
            if (!owners.Any() || string.IsNullOrWhiteSpace(param))
                return;
            owners = owners;
        }

        public void UpdatbyDoubons(APH_RMD_RapprochementManuelleDoublons Doublons)
        {
            Update(Doublons);
        }



        public bool verificationDoublon(APH_RMD_RapprochementManuelleDoublonsParameters Parameters)
        {
            List<APH_RMI_RapprochementManuelleIncohernce> list = new List<APH_RMI_RapprochementManuelleIncohernce>();
            list = _repositoryContext.Incoherences.Where(o => o.RMI_DateTransaction.Value.Date.Equals(Parameters.RMD_DateTransaction)
            || Parameters.RMD_DateTransaction==null).ToList(); 
            if (list.Count>0) { return true; } else { return false; }
        }


        public void UpdatbyDoubonsObject(APH_RMD_RapprochementManuelleDoublonsParameters Parameters)
        {
            if (Parameters.RMD_Source == "Amen")
            {
                var Dou = _repositoryContext.Set<amen_transaction_payee>().Where(dou => dou.id.Equals(Parameters.RMD_ID)).FirstOrDefault();
                var V_Dou = _repositoryContext.Set<APH_RMD_RapprochementManuelleDoublons>().Where(dou => dou.RMD_ID.Equals(Parameters.RMD_ID)&& dou.RMD_Source =="Amen").FirstOrDefault();
                APH_HDR_HistoriqueDesRapprochements DoublonsHistorique = new APH_HDR_HistoriqueDesRapprochements()
                {
                    HDR_DateTransaction = Dou.date_transaction,
                    HDR_CodePartenaire = "Amen",
                    HDR_CT = Dou.technical_account,
                    HDR_CTRectifie = Dou.technical_account,
                    HDR_CIN = Dou.cin,
                    HDR_CINRectifie = Dou.cin,
                    HDR_OldStatus = "DUPLICATED",
                    HDR_NewStatus = Parameters.RMD_O_Decision,
                    HDR_MontantVerse = Dou.amount_credit,
                    HDR_DateRectification = DateTime.Now,
                    HDR_NomBackOffice = Parameters.RMD_UserModified,
                    HDR_Commentaire = Parameters.RMD_O_Commentaire,
                    HDR_RSM = V_Dou.RMD_RSM,
                    HDR_CreatedOn = DateTime.Now,
                    HDR_CreatedBy = Parameters.RMD_UserModified,
                    HDR_UpdatedOn = DateTime.Now,
                    HDR_UpdatedBy = Parameters.RMD_UserModified
                };
                Dou.status_transaction = Parameters.RMD_O_Decision;
                Dou.no_transaction = Dou.no_transaction.Count() < 25 ? Dou.no_transaction + "-D" : Dou.no_transaction.Substring(0, 17) + "-D" + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString() + DateTime.Now.Millisecond.ToString();
                if (!string.IsNullOrEmpty(Parameters.RMD_D_Decision) && (Parameters.RMD_D_Decision == "CORRECTED" || Parameters.RMD_D_Decision == "SUSPENDED"))
                {
                    var Dou_Doublon = _repositoryContext.Set<amen_transaction_payee>().Where(dou => dou.id.Equals(Parameters.RMD_ID_Doublons)).FirstOrDefault();
                    APH_HDR_HistoriqueDesRapprochements SecendDoublonsHistorique = new APH_HDR_HistoriqueDesRapprochements()
                    {
                        HDR_DateTransaction = Dou_Doublon.date_transaction,
                        HDR_CodePartenaire = "Amen",
                        HDR_CT = Dou_Doublon.technical_account,
                        HDR_CTRectifie = Dou_Doublon.technical_account,
                        HDR_CIN = Dou_Doublon.cin,
                        HDR_CINRectifie = Dou_Doublon.cin,
                        HDR_OldStatus = "DUPLICATED",
                        HDR_NewStatus = Parameters.RMD_D_Decision,
                        HDR_MontantVerse = Dou_Doublon.amount_credit,
                        HDR_DateRectification = DateTime.Now,
                        HDR_NomBackOffice = Parameters.RMD_UserModified,
                        HDR_Commentaire = Parameters.RMD_D_Commentaire,
                        HDR_RSM = V_Dou.RMD_RSM,
                        HDR_CreatedOn = DateTime.Now,
                        HDR_CreatedBy = Parameters.RMD_UserModified,
                        HDR_UpdatedOn = DateTime.Now,
                        HDR_UpdatedBy = Parameters.RMD_UserModified
                    };

                    Dou_Doublon.status_transaction = Parameters.RMD_D_Decision;
                    Dou_Doublon.no_transaction = Dou_Doublon.no_transaction.Count() < 25 ? Dou_Doublon.no_transaction + "-D": Dou_Doublon.no_transaction.Substring(0,17)+ "-D" + DateTime.Now.Hour.ToString()+DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString() + DateTime.Now.Millisecond.ToString();
                    _repositoryContext.Set<amen_transaction_payee>().Update(Dou_Doublon);
                    _repositoryContext.Set<APH_HDR_HistoriqueDesRapprochements>().Add(SecendDoublonsHistorique);

                }

                _repositoryContext.Set<amen_transaction_payee>().Update(Dou);


                _repositoryContext.Set<APH_HDR_HistoriqueDesRapprochements>().Add(DoublonsHistorique);
            }
            else if (Parameters.RMD_Source == "AWB")
            {
                var Dou = _repositoryContext.Set<attijeri_transaction_payee>().Where(dou => dou.id.Equals(Parameters.RMD_ID)).FirstOrDefault();
                var V_Dou = _repositoryContext.Set<APH_RMD_RapprochementManuelleDoublons>().Where(dou => dou.RMD_ID.Equals(Parameters.RMD_ID) && dou.RMD_Source =="AWB").FirstOrDefault();
                APH_HDR_HistoriqueDesRapprochements DoublonsHistorique = new APH_HDR_HistoriqueDesRapprochements()
                {
                    HDR_DateTransaction = Dou.date_transaction,
                    HDR_CodePartenaire = "AWB",
                    HDR_CT = Dou.technical_account,
                    HDR_CTRectifie = Dou.technical_account,
                    HDR_CIN = Dou.cin,
                    HDR_CINRectifie = Dou.cin,
                    HDR_OldStatus = "DUPLICATED",
                    HDR_NewStatus = Parameters.RMD_O_Decision,
                    HDR_MontantVerse = Dou.amount_monthly_installment,
                    HDR_DateRectification = DateTime.Now,
                    HDR_NomBackOffice = Parameters.RMD_UserModified,
                    HDR_Commentaire = Parameters.RMD_O_Commentaire,
                    HDR_RSM = V_Dou.RMD_RSM,
                    HDR_CreatedOn = DateTime.Now,
                    HDR_CreatedBy = Parameters.RMD_UserModified,
                    HDR_UpdatedOn = DateTime.Now,
                    HDR_UpdatedBy = Parameters.RMD_UserModified
                };
                Dou.status_transaction = Parameters.RMD_O_Decision;
                //Dou.no_transaction = Dou.no_transaction + "-DouO";
                if (!string.IsNullOrEmpty(Parameters.RMD_D_Decision) && (Parameters.RMD_D_Decision == "CORRECTED" || Parameters.RMD_D_Decision == "SUSPENDED"))
                {
                    var Dou_Doublon = _repositoryContext.Set<attijeri_transaction_payee>().Where(dou => dou.id.Equals(Parameters.RMD_ID_Doublons)).FirstOrDefault();
                    APH_HDR_HistoriqueDesRapprochements SecendDoublonsHistorique = new APH_HDR_HistoriqueDesRapprochements()
                    {
                        HDR_DateTransaction = Dou_Doublon.date_transaction,
                        HDR_CodePartenaire = "AWB",
                        HDR_CT = Dou_Doublon.technical_account,
                        HDR_CTRectifie = Dou_Doublon.technical_account,
                        HDR_CIN = Dou_Doublon.cin,
                        HDR_CINRectifie = Dou_Doublon.cin,
                        HDR_OldStatus = "DUPLICATED",
                        HDR_NewStatus = Parameters.RMD_D_Decision,
                        HDR_MontantVerse = Dou_Doublon.amount_monthly_installment,
                        HDR_DateRectification = DateTime.Now,
                        HDR_NomBackOffice = Parameters.RMD_UserModified,
                        HDR_Commentaire = Parameters.RMD_D_Commentaire,
                        HDR_RSM = V_Dou.RMD_RSM,
                        HDR_CreatedOn = DateTime.Now,
                        HDR_CreatedBy = Parameters.RMD_UserModified,
                        HDR_UpdatedOn = DateTime.Now,
                        HDR_UpdatedBy = Parameters.RMD_UserModified
                    };
                    Dou_Doublon.status_transaction = Parameters.RMD_D_Decision;
                    //Dou_Doublon.no_transaction = Dou_Doublon.no_transaction + "-DouD";
                    _repositoryContext.Set<attijeri_transaction_payee>().Update(Dou_Doublon);
                    _repositoryContext.Set<APH_HDR_HistoriqueDesRapprochements>().Add(SecendDoublonsHistorique);
                }
                _repositoryContext.Set<attijeri_transaction_payee>().Update(Dou);
                _repositoryContext.Set<APH_HDR_HistoriqueDesRapprochements>().Add(DoublonsHistorique);
            }


        }


    }
}
