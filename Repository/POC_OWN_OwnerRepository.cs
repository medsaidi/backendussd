﻿using Contracts;
using Entities;
using Entities.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Linq.Dynamic.Core;
namespace Repository
{
    public class POC_OWN_OwnerRepository : RepositoryBase<POC_OWN_Owner>, IPOC_OWN_OwnerRepository
    {
        private ISortHelper<POC_OWN_Owner> _sortHelper;
        public POC_OWN_OwnerRepository(RepositoryContext repositoryContext, ISortHelper<POC_OWN_Owner> sortHelper)
            : base(repositoryContext)
        {
            _sortHelper = sortHelper;
        }
        public IEnumerable<POC_OWN_Owner> GetAllOwners()
        {
            return FindAll()
                .OrderBy(ow => ow.OWN_Name)
                .ToList();
        }
        public PagedList<POC_OWN_Owner> GetOwners(POC_OWN_OwnerParameters ownerParameters)
        {
            var owners = FindByCondition(o => o.OWN_DateOfBirth.Year >= ownerParameters.MinYearOfBirth &&
                                o.OWN_DateOfBirth.Year <= ownerParameters.MaxYearOfBirth);
            SearchByName(ref owners, ownerParameters.Name);

            //var sortedOwners = _sortHelper.ApplySort(owners, ownerParameters.OrderBy);

            return PagedList<POC_OWN_Owner>.ToPagedList(owners,
                ownerParameters.PageNumber,
                ownerParameters.PageSize);
        }
        public POC_OWN_Owner GetOwnerById(string ownerId)
        {
            return FindByCondition(owner => owner.ID.Equals(ownerId))
                    .FirstOrDefault();
        }
        public POC_OWN_Owner GetOwnerWithDetails(string ownerId)
        {
            return FindByCondition(owner => owner.ID.Equals(ownerId))
                .Include(ac => ac.OWN_Accounts)
                .FirstOrDefault();
        }
        public void CreateOwner(POC_OWN_Owner owner)
        {
            Create(owner);
        }
        public void UpdateOwner(POC_OWN_Owner owner)
        {
            Update(owner);
        }
        public void DeleteOwner(POC_OWN_Owner owner)
        {
            Delete(owner);
        }
        private void SearchByName(ref IQueryable<POC_OWN_Owner> owners, string ownerName)
        {
            if (!owners.Any() || string.IsNullOrWhiteSpace(ownerName))
                return;
            owners = owners.Where(o => o.OWN_Name.ToLower().Contains(ownerName.Trim().ToLower()));
        }
      
    }
}
