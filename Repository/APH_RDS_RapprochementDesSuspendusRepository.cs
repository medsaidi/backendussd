﻿using Contracts;
using Entities;
using Entities.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Linq.Dynamic.Core;
using System.Linq.Expressions;

namespace Repository
{
    public class APH_RDS_RapprochementDesSuspendusRepository : RepositoryBase<APH_RDS_RapprochementDesSuspendus>, IAPH_RDS_RapprochementDesSuspendusRepository
    {
        private ISortHelper<APH_RDS_RapprochementDesSuspendus> _sortHelper;
        private RepositoryContext _repositoryContext;
        public APH_RDS_RapprochementDesSuspendusRepository(RepositoryContext repositoryContext, ISortHelper<APH_RDS_RapprochementDesSuspendus> sortHelper)
            : base(repositoryContext)
        {
            _sortHelper = sortHelper;
            _repositoryContext = repositoryContext;
        }

        public void CreateSuspendus(APH_RDS_RapprochementDesSuspendus suspendus)
        {
            Create(suspendus);
        }



        public PagedList<APH_RDS_RapprochementDesSuspendus> GetSuspendus(APH_RDS_RapprochementDesSuspendusParameters Parameters)
        {
            var returnedList = FindByCondition(
                o =>
                    "true" == "true"
            );

            //SearchByName(ref returnedList, Parameters);

            //var filter = new Expression<Func<APH_RDS_RapprochementDesSuspendus, bool>>();

            var  _incoherenceItems =
                _repositoryContext.Set<APH_RDS_RapprochementDesSuspendus>()
                    .Where(
                        x =>
                            x.RDS_Source.ToLower()
                                .Contains(Parameters.RDS_Source)
                            || string.IsNullOrEmpty(Parameters.RDS_Source)
                     )
                    .Where(
                        x =>
                            x.RDS_DateTransaction.Equals(Parameters.RDS_DateTransaction)
                            || Parameters.RDS_DateTransaction == null
                     )
                    .AsNoTracking()
                    .AsQueryable()
                    .OrderByDescending(x => x.RDS_DateTransaction);
            return PagedList<APH_RDS_RapprochementDesSuspendus>
                    .ToPagedList(_incoherenceItems, Parameters.PageNumber, 500);
        }
     
        public APH_RDS_RapprochementDesSuspendus GetSuspendusById(int IdSuspendus)
        {

            return FindByCondition(sus => sus.RDS_ID.Equals(IdSuspendus))
                 .FirstOrDefault();

        }

        public void UpdateSuspendus(APH_RDS_RapprochementDesSuspendus suspendus)
        {
            Update(suspendus);
        }

        //public APH_RDS_RapprochementDesSuspendus UpdateSuspendusObject(int id, string cin, string ct, string commentaire, string decision)
        //{


        //        var Sus = GetSuspendusById(id);
        //        Sus.RDS_CIN = cin;
        //        Sus.RDS_CompteTechnique = ct;
        //        Sus.RDS_Commentaire = commentaire;
        //        Sus.RDS_Decision = decision;
        //        Update(Sus);
        //        return Sus;
        //}

        public void UpdateSuspendusObject(APH_RDS_RapprochementDesSuspendusParameters Parameters)
        {

            if (Parameters.RDS_Source=="Amen")
            {
                var Sus = _repositoryContext.Set<amen_transaction_payee>().Where(dou => dou.id.Equals(Parameters.RDS_ID)).FirstOrDefault();
                var V_Sus = _repositoryContext.Set<APH_RDS_RapprochementDesSuspendus>().Where(dou => dou.RDS_ID.Equals(Parameters.RDS_ID)&& dou.RDS_Source == "Amen").FirstOrDefault();
                APH_HDR_HistoriqueDesRapprochements SuspensHistorique = new APH_HDR_HistoriqueDesRapprochements()
                {
                    HDR_DateTransaction = Sus.date_transaction,
                    HDR_CodePartenaire = "Amen",
                    HDR_CT = Sus.technical_account,
                    HDR_CTRectifie = Parameters.RDS_CT,
                    HDR_CIN = Sus.cin,
                    HDR_CINRectifie = Parameters.RDS_CIN,
                    HDR_OldStatus = "SUSPENDED",
                    HDR_NewStatus = "SUSPENDED_CORRECTED",
                    HDR_MontantVerse = Sus.amount_credit,
                    HDR_DateRectification = DateTime.Now,
                    HDR_NomBackOffice = Parameters.RDS_UserModified,
                    HDR_Commentaire = Parameters.RDS_Commentaire,
                    HDR_RSM = V_Sus.RDS_CIN_RSM,
                    HDR_CreatedOn = DateTime.Now,
                    HDR_CreatedBy = Parameters.RDS_UserModified,
                    HDR_UpdatedOn = DateTime.Now,
                    HDR_UpdatedBy = Parameters.RDS_UserModified
                };
                Sus.cin = Parameters.RDS_CIN;
                Sus.technical_account = Parameters.RDS_CT;
                //Sus. = Parameters.RDS_Commentaire;
                Sus.status_transaction = "SUSPENDED_CORRECTED";
                _repositoryContext.Set<amen_transaction_payee>().Update(Sus);
                _repositoryContext.Set<APH_HDR_HistoriqueDesRapprochements>().Add(SuspensHistorique);
                _repositoryContext.SaveChanges();
            }
            else if (Parameters.RDS_Source =="AWB")
            {
                var Sus = _repositoryContext.Set<attijeri_transaction_payee>().Where(dou => dou.id.Equals(Parameters.RDS_ID)).FirstOrDefault();
                var V_Sus = _repositoryContext.Set<APH_RDS_RapprochementDesSuspendus>().Where(dou => dou.RDS_ID.Equals(Parameters.RDS_ID) && dou.RDS_Source =="AWB").FirstOrDefault();
                APH_HDR_HistoriqueDesRapprochements SuspensHistorique = new APH_HDR_HistoriqueDesRapprochements()
                {
                    HDR_DateTransaction = Sus.date_transaction,
                    HDR_CodePartenaire = "AWB",
                    HDR_CT = Sus.technical_account,
                    HDR_CTRectifie = Parameters.RDS_CT,
                    HDR_CIN = Sus.cin,
                    HDR_CINRectifie = Parameters.RDS_CIN,
                    HDR_OldStatus = "SUSPENDED",
                    HDR_NewStatus = "SUSPENDED_CORRECTED",
                    HDR_MontantVerse = Sus.amount_monthly_installment,
                    HDR_DateRectification = DateTime.Now,
                    HDR_NomBackOffice = Parameters.RDS_UserModified,
                    HDR_Commentaire = Parameters.RDS_Commentaire,
                    HDR_RSM = V_Sus.RDS_CIN_RSM,
                    HDR_CreatedOn = DateTime.Now,
                    HDR_CreatedBy = Parameters.RDS_UserModified,
                    HDR_UpdatedOn = DateTime.Now,
                    HDR_UpdatedBy = Parameters.RDS_UserModified
                };
                Sus.cin = Parameters.RDS_CIN;
                Sus.technical_account = Parameters.RDS_CT;
                //Sus. = Parameters.RDS_Commentaire;
                Sus.status_transaction = "SUSPENDED_CORRECTED";
                _repositoryContext.Set<attijeri_transaction_payee>().Update(Sus);
                _repositoryContext.Set<APH_HDR_HistoriqueDesRapprochements>().Add(SuspensHistorique);
                _repositoryContext.SaveChanges();
            }
            //var Sus = GetSuspendusById(Parameters.RDS_ID);
            
        }

        public bool VerificationCoherence(APH_RDS_RapprochementDesSuspendusParameters Parameters)
        {
            var Sus = _repositoryContext.Set<APH_TDA_TranscoDonneesAmplitude>()
                .Where(tr => tr.CIN.Contains(Parameters.RDS_CIN.Trim())&& tr.TECHNICAL_ACCOUNT_ORBIT.Contains(Parameters.RDS_CT.Trim()))
                .FirstOrDefault();
            if (Sus != null){ return true; } else { return false; }
        }

        //private void SearchByName(ref IQueryable<APH_RDS_RapprochementDesSuspendus> owners, APH_RDS_RapprochementDesSuspendusParameters parameters )
        //{
        //    bool ok = false; 

        //      if(!owners.Any() || (parameters.DateOp != null && parameters.Partenaire != "Tous") && ok==false)
        //    {
        //        owners = owners.Where(o => o.RDS_DateTransaction.Equals(parameters.DateOp))
        //                       .Where(o => o.RDS_Source.Equals(parameters.Partenaire.Trim()));
        //        ok = true; 
        //    }
        //      if (!owners.Any() ||  (parameters.DateOp != null && (parameters.Partenaire == "Tous")) && ok == false)
        //    {
        //        owners = owners.Where(o => o.RDS_DateTransaction.Equals(parameters.DateOp));
        //        ok = true; 
        //    }

        //    if (!owners.Any() || (parameters.Partenaire != "Tous" && parameters.DateOp==null ) && ok == false)
        //    {
        //        owners = owners.Where(o => o.RDS_Source.Equals(parameters.Partenaire.Trim()));
        //        ok = true; 
        //    }

        //    if (!owners.Any() || (parameters.Partenaire == "Tous" && parameters.DateOp == null) && ok == false)
        //    {
        //        owners = owners.AsQueryable();
        //        ok = true;
        //    }

        //}


    }
}