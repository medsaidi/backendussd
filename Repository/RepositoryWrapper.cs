﻿using Contracts;
using Entities;
using Entities.Models;

namespace Repository
{
    public class RepositoryWrapper : IRepositoryWrapper
    {
        private RepositoryContext _repoContext;
        private IPOC_OWN_OwnerRepository _owner;
        private IPOC_ACC_AccountRepository _account;
        private IAPH_V_Monitoring_PartenaireRepository _monitoring;
        private IAPH_RMI_RapprochementManuelleIncohernceRepository _incoherence;
        private IAPH_RMD_RapprochementManuelleDoublonsRepository _doublons;
        private IAPH_RDS_RapprochementDesSuspendusRepository _suspendus;
        private IAPH_HDR_HistoriqueDesRapprochementsRepository _historique;
        private IAPH_ADT_AnnulataionDesTransacationsRepository _annulation;
        private IAPH_VER_VersementEnRouteRepository _Versement;
        private IAPH_CJF_CalendrierJourFerieRepository _JourFerie; 
       private ISortHelper<POC_OWN_Owner> _ownerSortHelper;
        private ISortHelper<POC_ACC_Account> _accountSortHelper;
        private ISortHelper<APH_V_Monitoring_Partenaire> _monitoringSortHelper;
        private ISortHelper<APH_RMI_RapprochementManuelleIncohernce> _incoherenceSortHelper;
        private ISortHelper<APH_RMD_RapprochementManuelleDoublons> _doublonsSortHelper;
        private ISortHelper<APH_RDS_RapprochementDesSuspendus> _suspendusSortHelper;
        private ISortHelper<APH_HDR_HistoriqueDesRapprochements> _historiqueSortHelper;
        private ISortHelper<APH_ADT_AnnulataionDesTransacations> _annulationSortHelper;
        private ISortHelper<APH_VER_VersementEnRoute> _VersementSortHelper;
        private ISortHelper<APH_CJF_CalendrierJourFerie> _JourFerieSortHelper;


        private IClientsRepository _clients;
        private ITransactionsClientRepository _transactionsClient;
        private ISortHelper<Client> _clientsSortHelper;
        private ISortHelper<TransactionsClient> _transactionsClientSortHelper;
        private IClientInfosRepository _clientInfos;

       public IPOC_OWN_OwnerRepository Owner
        {
            get
            {
                if (_owner == null)
                {
                    _owner = new POC_OWN_OwnerRepository(_repoContext, _ownerSortHelper);
                }
                return _owner;
            }
        }

        public IPOC_ACC_AccountRepository Account
        {
            get
            {
                if (_account == null)
                {
                    _account = new POC_ACC_AccountRepository(_repoContext, _accountSortHelper);
                }
                return _account;
            }
        }
        public IAPH_V_Monitoring_PartenaireRepository Monitoring
        {
            get
            {
                if (_monitoring == null)
                {
                    _monitoring = new APH_V_Monitoring_PartenaireRepository(_repoContext, _monitoringSortHelper);
                }
                return _monitoring;
            }
        }
        public IAPH_RMI_RapprochementManuelleIncohernceRepository Incoherences
        {
            get
            {
                if (_incoherence == null)
                {
                    _incoherence = new APH_RMI_RapprochementManuelleIncohernceRepository(_repoContext, _incoherenceSortHelper);
                }
                return _incoherence;
            }
        }
        public IAPH_RDS_RapprochementDesSuspendusRepository Suspendus
        {
            get
            {
                if (_suspendus == null)
                {
                    _suspendus = new APH_RDS_RapprochementDesSuspendusRepository(_repoContext, _suspendusSortHelper);
                }
                return _suspendus;
            }
        }
        public IAPH_ADT_AnnulataionDesTransacationsRepository Annulation
        {
            get
            {
                if (_annulation == null)
                {
                    _annulation = new APH_ADT_AnnulataionDesTransacationsRepository(_repoContext, _annulationSortHelper);
                }
                return _annulation;
            }
        }
        public IAPH_RMD_RapprochementManuelleDoublonsRepository Doublons
        {
            get
            {
                if (_doublons == null)
                {
                    _doublons = new APH_RMD_RapprochementManuelleDoublonsRepository(_repoContext, _doublonsSortHelper);
                }
                return _doublons;
            }
        }

        public IAPH_VER_VersementEnRouteRepository VersementEnRoutes
        {
            get
            {
                if (_Versement == null)
                {
                    _Versement = new APH_VER_VersementEnRouteRepository(_repoContext, _VersementSortHelper);
                }
                return _Versement;
            }
        }
        public IAPH_HDR_HistoriqueDesRapprochementsRepository Historique
        {
            get
            {
                if (_historique == null)
                {
                    _historique = new APH_HDR_HistoriqueDesRapprochementsRepository(_repoContext, _historiqueSortHelper);
                }
                return _historique;
            }
        }

        public IAPH_CJF_CalendrierJourFerieRepository CalendrierJourFerie
        {
            get
            {
                if (_JourFerie == null)
                {
                    _JourFerie = new APH_CJF_CalendrierJourFerieRepository(_repoContext, _JourFerieSortHelper);
                }
                return _JourFerie;
            }
        }

        public IClientsRepository Clients
        {
            get
            {
                if (_clients == null) _clients = new ClientsRepository(_repoContext, _clientsSortHelper);
                return _clients;
            }
        }

        public ITransactionsClientRepository TransactionsClients
        {
            get
            {
                if (_transactionsClient == null)
                    _transactionsClient = new TransactionsClientRepository(_repoContext, _transactionsClientSortHelper);
                return _transactionsClient;
            }
        }

        public IClientInfosRepository ClientInfos
        {
            get
            {
                if (_clientInfos == null)
                    _clientInfos = new ClientInfosRepository(_repoContext);
                return _clientInfos;
            }
        }

        public RepositoryWrapper(
            RepositoryContext repositoryContext,
            ISortHelper<POC_OWN_Owner> ownerSortHelper,
            ISortHelper<POC_ACC_Account> accountSortHelper,
            ISortHelper<APH_V_Monitoring_Partenaire> monitoringSortHelper,
            ISortHelper<APH_RMI_RapprochementManuelleIncohernce> incoherenceSortHelper,
            ISortHelper<APH_RMD_RapprochementManuelleDoublons> doublonsSortHelper,
            ISortHelper<APH_RDS_RapprochementDesSuspendus> suspendusSortHelper,
            ISortHelper<APH_HDR_HistoriqueDesRapprochements> historiqueSortHelper,
            ISortHelper<APH_VER_VersementEnRoute> VersementSortHelper,
            ISortHelper<APH_ADT_AnnulataionDesTransacations> AnnulationSortHelper,
             ISortHelper<APH_CJF_CalendrierJourFerie> JourFerieSortHelper
        )
        {
            _repoContext = repositoryContext;
            _ownerSortHelper = ownerSortHelper;
            _accountSortHelper = accountSortHelper;
            _monitoringSortHelper = monitoringSortHelper;
            _incoherenceSortHelper = incoherenceSortHelper;
            _doublonsSortHelper = doublonsSortHelper;
            _suspendusSortHelper = suspendusSortHelper;
            _historiqueSortHelper = historiqueSortHelper;
            _VersementSortHelper = VersementSortHelper;
            _annulationSortHelper = AnnulationSortHelper;
            _JourFerieSortHelper = JourFerieSortHelper;
        }

        public void Save()
        {
            _repoContext.SaveChanges();
        }
    }
}
