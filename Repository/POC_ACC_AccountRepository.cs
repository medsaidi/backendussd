﻿using Contracts;
using Entities;
using Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Repository
{
    public class POC_ACC_AccountRepository : RepositoryBase<POC_ACC_Account>, IPOC_ACC_AccountRepository
    {
        private ISortHelper<POC_ACC_Account> _sortHelper;
        public POC_ACC_AccountRepository(RepositoryContext repositoryContext, ISortHelper<POC_ACC_Account> sortHelper)
            : base(repositoryContext)
        {
            _sortHelper = sortHelper;
        }
        public PagedList<POC_ACC_Account> GetAccountsByOwner(string ownerId, POC_ACC_AccountParameters parameters)
        {
            var accounts = FindByCondition(a => a.ACC_OwnerID.Equals(ownerId));
            var a = FindByCondition(a => a.ACC_OwnerID.Equals(ownerId)).ToList();
            return PagedList<POC_ACC_Account>.ToPagedList(accounts,
                parameters.PageNumber,
                parameters.PageSize);
        }
        public IEnumerable<POC_ACC_Account> AccountsByOwner(string ownerId)
        {
            return FindByCondition(a => a.ACC_OwnerID.Equals(ownerId)).ToList();
        }
    }
}
