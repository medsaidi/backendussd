﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using ClosedXML.Excel;
using Contracts;
using Entities;
using Entities.Models;
using Microsoft.EntityFrameworkCore;

namespace Repository
{
    public class ClientsRepository : RepositoryBase<Client>, IClientsRepository
    {
        private readonly RepositoryContext _repositoryContext;

        public ClientsRepository(RepositoryContext repositoryContext, ISortHelper<Client> sortHelper) : base(
            repositoryContext)
        {
            _repositoryContext = repositoryContext;
        }

        public PagedList<Client> Get(ClientParameters parameters)
        {
            var clients = _repositoryContext.Set<Client>()
                .Where(_ =>
                    string.IsNullOrWhiteSpace(parameters.AmplitudeId) || _.AmplitudeId == parameters.AmplitudeId)
                .Where(_ => string.IsNullOrWhiteSpace(parameters.Branch) ||
                            _.Branch == parameters.Branch)
                .Where(_ =>
                    string.IsNullOrWhiteSpace(parameters.Phone) ||
                    _.PhoneNumber == parameters.Phone)
                .AsNoTracking()
                .AsQueryable();
            var sortedItem = clients.OrderBy(_ => _.DateCreated);
            return PagedList<Client>
                .ToPagedList(sortedItem,
                    parameters.PageNumber,
                    /*parameters.PageSize*/500);
        }

        public List<string> GetAllBranch()
        {
            return _repositoryContext.Set<Client>().Select(_ => _.Branch).Distinct().ToList();
        }

        public Client[] GetToExport(ClientParameters parameters)
        {
            var clients = _repositoryContext.Set<Client>().Where(_ =>
                    string.IsNullOrWhiteSpace(parameters.AmplitudeId) || _.AmplitudeId == parameters.AmplitudeId)
                .Where(_ => string.IsNullOrWhiteSpace(parameters.Branch) ||
                            _.Branch == parameters.Branch)
                .Where(_ =>
                    string.IsNullOrWhiteSpace(parameters.Phone) ||
                    _.PhoneNumber == parameters.Phone)
                .AsNoTracking()
                .AsQueryable();
            var sortedItem = clients.OrderBy(_ => _.DateCreated).ToArray();
            return sortedItem;
            ;
        }

        public bool ResetPin(int clientId)
        {
            var client = FindByCondition(_ => _.Id.Equals(clientId))
                .FirstOrDefault();
            if (client != null)
            {
                client.PIN = "";

                _repositoryContext.Set<Client>().Update(client);
                return _repositoryContext.SaveChanges() == 1;
            }

            return false;
        }

        public MemoryStream MemoryStream(Client[] returnedList)
        {
            var workbook = new XLWorkbook();

            // Add a new worksheet to the workbook
            var worksheet = workbook.Worksheets.Add("My Worksheet");

            // Add headers to the worksheet
            worksheet.Cell(1, 1).Value = "AmplitudeId";
            worksheet.Cell(1, 2).Value = "FirstName";
            worksheet.Cell(1, 3).Value = "LastName";
            worksheet.Cell(1, 4).Value = "PhoneNumber";
            worksheet.Cell(1, 5).Value = "Branch";
            worksheet.Cell(1, 6).Value = "DateCreated";
            worksheet.Cell(1, 7).Value = "ussd ?";
            worksheet.Cell(1, 8).Value = "MobileMoney ?";

            // Add data to the worksheet
            for (var i = 0; i < returnedList.Length; i++)
            {
                worksheet.Cell(i + 2, 1).Value = returnedList[i].AmplitudeId;
                worksheet.Cell(i + 2, 2).Value = returnedList[i].FirstName;
                worksheet.Cell(i + 2, 3).Value = returnedList[i].LastName;
                worksheet.Cell(i + 2, 4).Value = returnedList[i].PhoneNumber;
                worksheet.Cell(i + 2, 5).Value = returnedList[i].Branch;
                worksheet.Cell(i + 2, 6).Value = returnedList[i].DateCreated;
                var ussd = returnedList[i]?.Ussd;
                worksheet.Cell(i + 2, 7).Value = ussd != null && (bool)ussd ? "Active" : "Inactive";
                worksheet.Cell(i + 2, 8).Value = returnedList[i].MobileMoney;
            }

            // Save the workbook to a memory stream
            var stream = new MemoryStream();
            workbook.SaveAs(stream);

            // Set the position of the memory stream to the beginning
            stream.Seek(0, SeekOrigin.Begin);
            return stream;
        }
    }
}