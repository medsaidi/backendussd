﻿using System.IO;
using System.Linq;
using ClosedXML.Excel;
using Contracts;
using Entities;
using Entities.Models;
using Microsoft.EntityFrameworkCore;

namespace Repository
{
    public class TransactionsClientRepository : RepositoryBase<TransactionsClient>, ITransactionsClientRepository
    {
        private readonly RepositoryContext _repositoryContext;

        public TransactionsClientRepository(RepositoryContext repositoryContext,
            ISortHelper<TransactionsClient> sortHelper) : base(repositoryContext)
        {
            _repositoryContext = repositoryContext;
        }

        public PagedList<TransactionsClient> GetTransactionsClients(TransactionsClientsParameters parameters)
        {
            var transactionsClients = _repositoryContext.Set<TransactionsClient>()
                .Where(_ =>
                    !parameters.FromDate.HasValue || _.Date >= parameters.FromDate)
                .Where(_ => !parameters.FromDate.HasValue ||
                            _.Date <= parameters.ToDate)
                .AsNoTracking()
                .AsQueryable();
            var sortedItem = transactionsClients.OrderBy(_ => _.Date);
            return PagedList<TransactionsClient>
                .ToPagedList(sortedItem,
                    parameters.PageNumber,
                    /*parameters.PageSize*/500);
        }

        public TransactionsClient[] GetTransactionsClientsToExport(TransactionsClientsParameters parameters)
        {
            var u = _repositoryContext.Set<TransactionsClient>()
                .Where(_ => !parameters.FromDate.HasValue || _.Date >= parameters.FromDate)
                .Where(_ => !parameters.FromDate.HasValue ||
                            _.Date <= parameters.ToDate)
                .AsNoTracking()
                .AsQueryable();
            var sortedItem = u.OrderBy(_ => _.Date);
            return sortedItem.ToArray();
        }

        public MemoryStream MemoryStream(TransactionsClient[] returnedList)
        {
            var workbook = new XLWorkbook();

            // Add a new worksheet to the workbook
            var worksheet = workbook.Worksheets.Add("My Worksheet");

            // Add headers to the worksheet
            worksheet.Cell(1, 1).Value = "BankId";
            worksheet.Cell(1, 2).Value = "TelcoId";
            worksheet.Cell(1, 3).Value = "Amount";
            worksheet.Cell(1, 4).Value = "Fee";
            worksheet.Cell(1, 5).Value = "Status";
            worksheet.Cell(1, 6).Value = "Reason";
            worksheet.Cell(1, 7).Value = "TransactionType";
            worksheet.Cell(1, 8).Value = "ClientName";
            worksheet.Cell(1, 9).Value = "OriginAccountNumber";
            worksheet.Cell(1, 10).Value = "DestinationAccountNumber";
            worksheet.Cell(1, 11).Value = "Branch";
            worksheet.Cell(1, 12).Value = "Date";
            worksheet.Cell(1, 13).Value = "Timestamp";
            worksheet.Cell(1, 14).Value = "Network";


            // Add data to the worksheet
            for (var i = 0; i < returnedList.Length; i++)
            {
                worksheet.Cell(i + 2, 1).Value = returnedList[i].BankId;
                worksheet.Cell(i + 2, 2).Value = returnedList[i].TelcoId;
                worksheet.Cell(i + 2, 3).Value = returnedList[i].Amount;
                worksheet.Cell(i + 2, 4).Value = returnedList[i].Fee;
                worksheet.Cell(i + 2, 5).Value =
                    returnedList[i].Status != null && (bool)returnedList[i].Status ? "Success" : "Failure";
                ;
                worksheet.Cell(i + 2, 6).Value = returnedList[i].Reason;
                worksheet.Cell(i + 2, 7).Value = returnedList[i].TransactionType;
                worksheet.Cell(i + 2, 8).Value = returnedList[i].ClientName;
                worksheet.Cell(i + 2, 9).Value = returnedList[i].OriginAccountNumber;
                worksheet.Cell(i + 2, 10).Value = returnedList[i].DestinationAccountNumber;
                worksheet.Cell(i + 2, 11).Value = returnedList[i].Branch;
                var dateTime = returnedList[i].Date;
                if (dateTime != null)
                {
                    worksheet.Cell(i + 2, 12).Value = dateTime.Value.Date;
                    worksheet.Cell(i + 2, 13).Value = dateTime.Value.TimeOfDay;
                }
                else
                {
                    worksheet.Cell(i + 2, 12).Value = "";
                    worksheet.Cell(i + 2, 13).Value = "";
                }

                worksheet.Cell(i + 2, 14).Value = returnedList[i].Network;
            }

            // Save the workbook to a memory stream
            var stream = new MemoryStream();
            workbook.SaveAs(stream);

            // Set the position of the memory stream to the beginning
            stream.Seek(0, SeekOrigin.Begin);
            return stream;
        }
    }
}