﻿using Contracts;
using Entities;
using Entities.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Linq.Dynamic.Core;
namespace Repository
{
    public class APH_HDR_HistoriqueDesRapprochementsRepository : RepositoryBase<APH_HDR_HistoriqueDesRapprochements>, IAPH_HDR_HistoriqueDesRapprochementsRepository
    {
        private ISortHelper<APH_HDR_HistoriqueDesRapprochements> _sortHelper;
        private RepositoryContext _repositoryContext;
        public APH_HDR_HistoriqueDesRapprochementsRepository(RepositoryContext repositoryContext, ISortHelper<APH_HDR_HistoriqueDesRapprochements> sortHelper)
            : base(repositoryContext)
        {
            _sortHelper = sortHelper;
            _repositoryContext = repositoryContext;
           
    }

        public PagedList<APH_HDR_HistoriqueDesRapprochements> GetHistorique(APH_HDR_HistoriqueDesRapprochementsParameters Parameters)
        {
            var returnedList = FindByCondition(
                o =>
                    "true" == "true"
            );
            //SearchByName(ref returnedList, Parameters.Name);
            //var sortedOwners = _sortHelper.ApplySort(owners, ownerParameters.OrderBy);

            var _incoherenceItems =
                _repositoryContext.Set<APH_HDR_HistoriqueDesRapprochements>()
                    //.Where(
                    //    x =>
                    //        x.HDR_CodePartenaire.ToLower()
                    //            .Contains(Parameters.HDR_CodePartenaire)
                    //        || string.IsNullOrEmpty(Parameters.HDR_CodePartenaire)
                    // )
                    ////.Where(
                    ////    x =>
                    ////        (x.HDR_DateTransaction.Value.Date >=  (Parameters.HDR_DateOperation) && x.HDR_DateTransaction.Value.Date <= (Parameters.HDR_DateOperation_Fin))
                    ////        || (Parameters.HDR_DateOperation == null && Parameters.HDR_DateOperation_Fin == null) ||
                    ////        (Parameters.HDR_DateOperation==null && x.HDR_DateTransaction.Value.Date <= (Parameters.HDR_DateOperation_Fin))
                    ////        || (Parameters.HDR_DateOperation_Fin == null && x.HDR_DateTransaction.Value.Date >= (Parameters.HDR_DateOperation))
                    //// )
                    //.Where(
                    //    x => 
                    //        x.HDR_DateRectification.Value.Date >= Parameters.HDR_DateOperation || !Parameters.HDR_DateOperation.HasValue
                    //)
                    //.Where(
                    //    x =>
                    //        x.HDR_DateRectification.Value.Date <= Parameters.HDR_DateOperation_Fin|| !Parameters.HDR_DateOperation_Fin.HasValue
                    //)
                    .AsNoTracking()
                    .AsQueryable();
            var sorteditem = _incoherenceItems.OrderByDescending(x => x.HDR_DateRectification);
            return PagedList<APH_HDR_HistoriqueDesRapprochements>
                    .ToPagedList(sorteditem, Parameters.PageNumber, 500);
        }

        //private void SearchByName(ref IQueryable<APH_V_Monitoring_Partenaire> owners, string param)
        //{
        //    if (!owners.Any() || string.IsNullOrWhiteSpace(param))
        //        return;
        //    owners = owners.Where(o => o.NomBatch.ToLower().Contains(param.Trim().ToLower()));
        //}

    }
}