﻿using Contracts;
using Entities;
using Entities.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Linq.Dynamic.Core;
namespace Repository
{
    public class APH_ADT_AnnulataionDesTransacationsRepository : RepositoryBase<APH_ADT_AnnulataionDesTransacations>, IAPH_ADT_AnnulataionDesTransacationsRepository
    {
        private ISortHelper<APH_ADT_AnnulataionDesTransacations> _sortHelper;
        private RepositoryContext _repositoryContext;
        public APH_ADT_AnnulataionDesTransacationsRepository(RepositoryContext repositoryContext, ISortHelper<APH_ADT_AnnulataionDesTransacations> sortHelper)
            : base(repositoryContext)
        {
            _sortHelper = sortHelper;
            _repositoryContext = repositoryContext;
        }

        public PagedList<APH_ADT_AnnulataionDesTransacations> GetAnnulation(APH_ADT_AnnulataionDesTransacationsParameters Parameters)
        {
            var returnedList = FindByCondition(
                o =>
                    "true" == "true"
            ).OrderByDescending(x => x.ADT_DateTransaction);//.Where(x => x.ADT_Decision == null);
            //SearchByName(ref returnedList, Parameters.Name);
            //var sortedOwners = _sortHelper.ApplySort(owners, ownerParameters.OrderBy);
    


            var _incoherenceItems =
             _repositoryContext.Set<APH_ADT_AnnulataionDesTransacations>()
                 .Where(
                     x =>
                         x.ADT_CodePartenaire.ToLower()
                             .Contains(Parameters.Partenaire)
                         || string.IsNullOrEmpty(Parameters.Partenaire)
                  )
                 .Where(
                     x =>
                         x.ADT_DateTransaction.Equals(Parameters.ADT_DateOperation)
                         || Parameters.ADT_DateOperation == null
                  )
                 .AsNoTracking()
                 .AsQueryable()
                 .OrderByDescending(x => x.ADT_DateTransaction);
            return PagedList<APH_ADT_AnnulataionDesTransacations>
                    .ToPagedList(_incoherenceItems, 1, 500);
        }

        public APH_ADT_AnnulataionDesTransacations GetAnnulationById(int idAnnulation)
        {
            return FindByCondition(ann => ann.ADT_ID.Equals(idAnnulation))
                 .FirstOrDefault();
        }

        public void UpdateAnnulation(APH_ADT_AnnulataionDesTransacations annulation)
        {
            Update(annulation);
        }

        public APH_ADT_AnnulataionDesTransacations UpdateAnnulationObject(APH_ADT_AnnulataionDesTransacationsParameters Parameters)
        {
            var ann = _repositoryContext.Set<amen_transaction_payee>().Where(dou => dou.id.Equals(Parameters.ADT_ID)).FirstOrDefault();
            ann.status_transaction = "IGNORED";
            var Ann = GetAnnulationById(Parameters.ADT_ID);
            //// //ann.ADT_CIN = Parameters.ADT_CIN;
            ////// ann.ADT_CompteTechnique = Parameters.RDS_CompteTechnique;
            //// //ann.ADT_Commentaire = Parameters.RDS_Commentaire;
            //// //Ann.ADT_Decision = Parameters.ADT_Decision;
            _repositoryContext.Set<amen_transaction_payee>().Update(ann);
            _repositoryContext.SaveChanges();
            return Ann;
            //return null;
        }

        public void UpdateArray(APH_ADT_AnnulataionDesTransacationsParameters[] tables)
        {
            List<amen_transaction_payee> liste = new List<amen_transaction_payee>();
            for (int i = 0; i < tables.Length; i++)
            {
                var Ver = _repositoryContext.Set<amen_transaction_payee>().Where(dou => dou.id.Equals(tables[i].ADT_ID)).FirstOrDefault();
                Ver.status_transaction = "IGNORED";
                liste.Add(Ver);

                APH_HDR_HistoriqueDesRapprochements Historique = new APH_HDR_HistoriqueDesRapprochements()
                {
                    HDR_DateTransaction = Ver.date_transaction,
                    HDR_CodePartenaire = "Amen",
                    HDR_CT = Ver.technical_account,
                    HDR_CTRectifie = "",
                    HDR_CIN = Ver.cin,
                    HDR_CINRectifie = "",
                    HDR_OldStatus = "ANNULATION",
                    HDR_NewStatus = Ver.status_transaction,
                    HDR_MontantVerse = Ver.amount_credit,
                    HDR_DateRectification = DateTime.Now,
                    HDR_NomBackOffice = tables[i].ADT_UserModified,
                    HDR_Commentaire = "",
                    //HDR_RSM = V_Inco.,
                    HDR_CreatedOn = DateTime.Now,
                    HDR_CreatedBy = tables[i].ADT_UserModified,
                    HDR_UpdatedOn = DateTime.Now,
                    HDR_UpdatedBy = tables[i].ADT_UserModified
                };
                _repositoryContext.Set<APH_HDR_HistoriqueDesRapprochements>().Add(Historique);
            }
            _repositoryContext.Set<amen_transaction_payee>().UpdateRange(liste);
            _repositoryContext.SaveChanges();
        }
    }
}
