﻿using Contracts;
using Entities;
using Entities.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Linq.Dynamic.Core;
using FuzzySharp;
using System.Linq.Expressions;

namespace Repository
{
    public class APH_RMI_RapprochementManuelleIncohernceRepository : RepositoryBase<APH_RMI_RapprochementManuelleIncohernce>, IAPH_RMI_RapprochementManuelleIncohernceRepository
    {
        private ISortHelper<APH_RMI_RapprochementManuelleIncohernce> _sortHelper;
        private RepositoryContext _repoContext;
        public APH_RMI_RapprochementManuelleIncohernceRepository(RepositoryContext repositoryContext, ISortHelper<APH_RMI_RapprochementManuelleIncohernce> sortHelper)
            : base(repositoryContext)
        {
            _sortHelper = sortHelper;
            _repoContext = repositoryContext;
        }
        public IList<APH_TDA_TranscoDonneesAmplitude> GetCorrectionAutomatique(APH_RMI_RapprochementManuelleIncohernceParameters ownerParameters)
        {
            IList<APH_TDA_TranscoDonneesAmplitude> _transcoItems = _repoContext.Set<APH_TDA_TranscoDonneesAmplitude>().Where(x => x.TECHNICAL_ACCOUNT_ORBIT != null).AsNoTracking().ToList();
            IList<APH_RMI_RapprochementManuelleIncohernce> _incoherenceItems = _repoContext.Set<APH_RMI_RapprochementManuelleIncohernce>().Where(x => x.RMI_ISC_Percentage == null && x.RMI_DateInsertion > DateTime.Now.AddDays(-1)).AsNoTracking().ToList();
            //IList<APH_RMI_RapprochementManuelleIncohernce> _incoherenceItems = _repoContext.Set<APH_RMI_RapprochementManuelleIncohernce>().Where(x => x.RMI_SA_Suggestion == false).AsNoTracking().ToList();
            foreach (var item in _incoherenceItems)
            {
                APH_ISC_IncoherenceSuggestedCorrection Correction = new APH_ISC_IncoherenceSuggestedCorrection();
                Correction.ISC_TransactionId = item.RMI_TransactionId;
                bool exsistCT = _transcoItems.Select(x => x.TECHNICAL_ACCOUNT_ORBIT.Trim()).Contains(item.RMI_CT);
                bool exsistCIN = _transcoItems.Select(x => x.CIN.Trim()).Contains(item.RMI_CIN);
                APH_TDA_TranscoDonneesAmplitude searchCT = _repoContext.Set<APH_TDA_TranscoDonneesAmplitude>().Where(x => x.TECHNICAL_ACCOUNT_ORBIT.Trim() == item.RMI_CT).FirstOrDefault();
                //APH_TDA_TranscoDonneesAmplitude searchCin = _repoContext.Set<APH_TDA_TranscoDonneesAmplitude>().Where(x => x.CIN.Trim() == item.RMI_CIN).FirstOrDefault();
                string EtatCt = searchCT != null ? searchCT.ACCOUNT_CLOSE : "NotFound";
                string relatedCinToCt = searchCT != null ? searchCT.CIN : "NotFound";
                if (exsistCT && EtatCt == "O" && relatedCinToCt == item.RMI_CIN)
                {
                    APH_TDA_TranscoDonneesAmplitude openedCt = _repoContext.Set<APH_TDA_TranscoDonneesAmplitude>().Where(x => x.CUSTOMERID.Trim() == item.RMI_CT_RIM && x.ACCOUNT_CLOSE == "N").FirstOrDefault();
                    if (openedCt != null)
                    {
                        Correction.ISC_CinSuggested = openedCt.CIN.Trim();
                        Correction.ISC_CtSuggested = openedCt.TECHNICAL_ACCOUNT_ORBIT.Trim();
                        Correction.ISC_Percenatge = "100";
                        Correction.ISC_Raison = "CT fermer";
                        Correction.ISC_ToDo = "Récupérer le nouveau CT depuis amplitude";
                        Correction.ISC_UpdatedBy = "SuggestionAutomatique";
                        Correction.ISC_UpdatedOn = DateTime.Now;
                    }
                    else
                    {
                        Correction.ISC_CinSuggested = "";
                        Correction.ISC_CtSuggested = "";
                        Correction.ISC_Percenatge = "0";
                        Correction.ISC_Raison = "CT fermer";
                        Correction.ISC_ToDo = "Pas de compte ouvert pour ce client dans amplitude";
                        Correction.ISC_UpdatedBy = "SuggestionAutomatique";
                        Correction.ISC_UpdatedOn = DateTime.Now;
                    }

                }
                else if (exsistCT && !exsistCIN)
                {
                    var cinSuggested = _transcoItems.Where(x => x.TECHNICAL_ACCOUNT_ORBIT.Trim() == item.RMI_CT && x.ACCOUNT_CLOSE == "N").Select(x => x.CIN.Trim()).FirstOrDefault();
                    if (string.IsNullOrEmpty(cinSuggested))
                    {
                        Correction.ISC_CinSuggested = "";
                        Correction.ISC_CtSuggested = item.RMI_CT;
                        Correction.ISC_Percenatge = "0";
                        Correction.ISC_Raison = "CT éxistant mais fermer, et le cin est inéexistant";
                        Correction.ISC_ToDo = "Pas de compte ouvert pour ce client dans amplitude";
                        Correction.ISC_UpdatedBy = "SuggestionAutomatique";
                        Correction.ISC_UpdatedOn = DateTime.Now;
                    }
                    else
                    {
                        Correction.ISC_CinSuggested = _transcoItems.Where(x => x.TECHNICAL_ACCOUNT_ORBIT.Trim() == item.RMI_CT && x.ACCOUNT_CLOSE == "N").Select(x => x.CIN.Trim()).FirstOrDefault();
                        Correction.ISC_CtSuggested = item.RMI_CT;
                        Correction.ISC_Percenatge = "100";
                        Correction.ISC_Raison = "CIN inéxistant";
                        Correction.ISC_ToDo = "Récupérer CT depuis amplitude";
                        Correction.ISC_UpdatedBy = "SuggestionAutomatique";
                        Correction.ISC_UpdatedOn = DateTime.Now;
                    }
                    //item.RMI_SA_Suggestion = true;
                    //Update(item);
                    //Update(Correction);
                }
                else if (!exsistCT && exsistCIN)
                {
                    Correction.ISC_CtSuggested = _transcoItems.Where(x => x.CIN.Trim() == item.RMI_CIN && x.ACCOUNT_CLOSE == "N").Select(x => x.TECHNICAL_ACCOUNT_ORBIT.Trim()).FirstOrDefault();
                    Correction.ISC_CinSuggested = item.RMI_CIN;
                    Correction.ISC_Percenatge = "100";
                    Correction.ISC_Raison = "CT inéxistant";
                    Correction.ISC_ToDo = "Récupérer CIN depuis amplitude";
                    Correction.ISC_UpdatedBy = "SuggestionAutomatique";
                    Correction.ISC_UpdatedOn = DateTime.Now;
                }
                else if (!exsistCT && !exsistCIN)
                {
                    int fuzzyScore = 0;
                    string suggestedCt = "";
                    string suggestedCin = "";
                    foreach (var Fuzzyelement in _transcoItems)
                    {
                        int elementFuzzyScore = Fuzz.Ratio(item.ConcatCtCin, Fuzzyelement.CIN.Trim() + Fuzzyelement.TECHNICAL_ACCOUNT_ORBIT.Trim());
                        if (elementFuzzyScore > fuzzyScore && Fuzzyelement.ACCOUNT_CLOSE == "N")
                        {
                            fuzzyScore = elementFuzzyScore;
                            suggestedCt = Fuzzyelement.TECHNICAL_ACCOUNT_ORBIT.Trim();
                            suggestedCin = Fuzzyelement.CIN.Trim();
                        }
                    }
                    Correction.ISC_CtSuggested = suggestedCt;
                    Correction.ISC_CinSuggested = suggestedCin;
                    Correction.ISC_Percenatge = fuzzyScore.ToString();
                    Correction.ISC_Raison = "CT et CIN inéxistant";
                    Correction.ISC_ToDo = "Rapprochement avec les couple depuis amplitude";
                    Correction.ISC_UpdatedBy = "SuggestionAutomatique";
                    Correction.ISC_UpdatedOn = DateTime.Now;

                }
                else if (exsistCT && exsistCIN)
                {
                    string suggestedCt = "";
                    string suggestedCin = "";
                    decimal score = 0;
                    APH_TDA_TranscoDonneesAmplitude CtrelatedTransco = _transcoItems.Where(x => x.TECHNICAL_ACCOUNT_ORBIT.Trim() == item.RMI_CT && x.ACCOUNT_CLOSE == "N").FirstOrDefault();
                    APH_TDA_TranscoDonneesAmplitude CinrelatedTransco = _transcoItems.Where(x => x.CIN.Trim() == item.RMI_CIN && x.ACCOUNT_CLOSE == "N").FirstOrDefault();
                    decimal DiffEcheanceCinAmplitude = item.RMI_DateTransaction != null ?
                        Math.Abs(decimal.Parse((CinrelatedTransco.DATE_MONTHLY_INSTALLEMENT.Value - item.RMI_DateTransaction.Value).TotalDays.ToString()))
                        : 0;
                    //int DiffEcheanceCinAmplitude = 0;
                    decimal DiffEcheanceCtAmplitude = 1000;
                    decimal DiffMontantEchCin = item.RMI_DateTransaction != null ?
                        Math.Abs(decimal.Parse(CinrelatedTransco.AMOUNT_MONTHLY_INSTALLEMENT.Trim().Replace(" ", "").Replace(".", ",")) - decimal.Parse(item.RMI_Ammount.Trim().Replace(" ", "").Replace(".", ",")))
                        : 0;
                    decimal DiffMontantEchCt = 10000;
                    if (CtrelatedTransco != null)
                    {
                        DiffEcheanceCtAmplitude = item.RMI_DateTransaction != null ?
                        Math.Abs(decimal.Parse((CtrelatedTransco.DATE_MONTHLY_INSTALLEMENT.Value - item.RMI_DateTransaction.Value).TotalDays.ToString()))
                        : 1000;
                        DiffMontantEchCt = item.RMI_DateTransaction != null ?
                        Math.Abs(decimal.Parse(CtrelatedTransco.AMOUNT_MONTHLY_INSTALLEMENT.Trim().Replace(" ", "").Replace(".", ",")) - decimal.Parse(item.RMI_Ammount.Trim().Replace(" ", "").Replace(".", ",")))
                        : 1000;
                    }
                    //int DiffEcheanceCtAmplitude = 0;
                    bool testMontant = true;
                    if (DiffMontantEchCin < DiffMontantEchCt)
                    {
                        if (DiffMontantEchCin < 50)
                        {
                            suggestedCt = CinrelatedTransco.TECHNICAL_ACCOUNT_ORBIT.Trim();
                            suggestedCin = CinrelatedTransco.CIN.Trim();
                            score = 75;
                            testMontant = false;
                        }
                    }
                    else if (DiffMontantEchCin > DiffMontantEchCt)
                        //if (true)
                    {
                        if (DiffMontantEchCt < 50)
                        {
                            suggestedCt = CtrelatedTransco.TECHNICAL_ACCOUNT_ORBIT.Trim();
                            suggestedCin = CtrelatedTransco.CIN.Trim();
                            score = 100;
                            testMontant = false;
                        }
                    }
                    if (testMontant && (DiffEcheanceCinAmplitude >= DiffEcheanceCtAmplitude))
                    {
                        suggestedCt = CtrelatedTransco.TECHNICAL_ACCOUNT_ORBIT.Trim();
                        suggestedCin = CtrelatedTransco.CIN.Trim();
                        score = 100 - (DiffEcheanceCtAmplitude * 2);
                    }
                    else if (testMontant && (DiffEcheanceCinAmplitude < DiffEcheanceCtAmplitude))
                    {
                        suggestedCt = CinrelatedTransco.TECHNICAL_ACCOUNT_ORBIT.Trim();
                        suggestedCin = CinrelatedTransco.CIN.Trim();
                        score = 100 - (DiffEcheanceCinAmplitude * 2);
                    }
                    else
                    {
                        // ToCheck
                    }
                    Correction.ISC_CtSuggested = suggestedCt;
                    Correction.ISC_CinSuggested = suggestedCin;
                    Correction.ISC_Percenatge = Math.Round(score, 2).ToString();
                    Correction.ISC_Raison = "CT et CIN existant mais incohérent";
                    Correction.ISC_ToDo = "Rapprochement avec la date d'échéance";
                    Correction.ISC_UpdatedBy = "SuggestionAutomatique";
                    Correction.ISC_UpdatedOn = DateTime.Now;
                }
                if (Correction != null)
                {
                    _repoContext.Set<APH_ISC_IncoherenceSuggestedCorrection>().Add(Correction);
                    _repoContext.SaveChanges();
                }
            }
            return _transcoItems;
            //return null;
        }

        public APH_RMI_RapprochementManuelleIncohernce GetDoublonsById(int idInc)
        {

            return FindByCondition(dou => dou.RMI_ID.Equals(idInc))
                 .FirstOrDefault();
        }
        public PagedList<APH_RMI_RapprochementManuelleIncohernce> GetIncoherence(APH_RMI_RapprochementManuelleIncohernceParameters Parameters)
        {
            var returnedList = FindByCondition(
                o =>
                    "true" == "true"
            );
            //SearchByName(ref returnedList, Parameters.Name);
            //var sortedOwners = _sortHelper.ApplySort(owners, ownerParameters.OrderBy);
            var _incoherenceItems =
                _repoContext.Set<APH_RMI_RapprochementManuelleIncohernce>()
                    .Where(
                        x =>
                            x.RMI_Source.ToLower()
                                .Contains(Parameters.RMI_Source)
                            || string.IsNullOrEmpty(Parameters.RMI_Source)
                     )
                    //.Where(
                    //    x =>
                    //        //x.RMI_DateTransaction.Value.Date == Parameters.RMI_DateTransaction
                    //        x.RMI_DateInsertion.Value.Date > DateTime.Now.AddDays(-2)
                    //        || Parameters.RMI_DateTransaction == null
                    // )
                    .AsNoTracking()
                    .AsQueryable()
                    .OrderByDescending(x => x.RMI_DateTransaction);
            return PagedList<APH_RMI_RapprochementManuelleIncohernce>
                    .ToPagedList(_incoherenceItems, 1, 500);
        }
        public void UpdateIncoherence(APH_RMI_RapprochementManuelleIncohernceParameters parametres)
        {
            if (parametres.RMI_Source == "Amen")
            {
                var Inco = _repoContext.Set<amen_transaction_payee>().Where(dou => dou.id.Equals(parametres.RMI_ID)).FirstOrDefault();
                var V_Inco = _repoContext.Set<APH_RMI_RapprochementManuelleIncohernce>().Where(dou => dou.RMI_ID.Equals(parametres.RMI_ID) && dou.RMI_Source == "Amen").FirstOrDefault();
                int CheckDouble = _repoContext.Set<amen_transaction_payee>()
                    .Where(dou =>
                        (dou.technical_account == parametres.RMI_ISC_CtSuggested) &&
                        (dou.cin == parametres.RMI_ISC_CinSuggested) &&
                        (dou.date_transaction.Value.Year == (Inco.date_transaction.Value.Year)) &&
                        (dou.date_transaction.Value.Month == (Inco.date_transaction.Value.Month)) &&
                        (dou.date_transaction.Value.Day == (Inco.date_transaction.Value.Day))
                    )
                    .ToList().Count;
                string NewStatus = "CORRECTED";
                if (CheckDouble >= 1)
                {
                    NewStatus = "DOUBLE";
                }
                APH_HDR_HistoriqueDesRapprochements IncoherenceHistorique = new APH_HDR_HistoriqueDesRapprochements()
                {
                    HDR_DateTransaction = Inco.date_transaction,
                    HDR_CodePartenaire = "Amen",
                    HDR_CT = Inco.technical_account,
                    HDR_CTRectifie = parametres.RMI_ISC_CtSuggested,
                    HDR_CIN = Inco.cin,
                    HDR_CINRectifie = parametres.RMI_ISC_CinSuggested,
                    HDR_OldStatus = "INCOHERENCE",
                    HDR_NewStatus = NewStatus,
                    HDR_MontantVerse = Inco.amount_credit,
                    HDR_DateRectification = DateTime.Now,
                    HDR_NomBackOffice = parametres.RMI_UserModified,
                    HDR_Commentaire = parametres.RMI_Commentaire,
                    //HDR_RSM = V_Inco.,
                    HDR_CreatedOn = DateTime.Now,
                    HDR_CreatedBy = parametres.RMI_UserModified,
                    HDR_UpdatedOn = DateTime.Now,
                    HDR_UpdatedBy = parametres.RMI_UserModified
                };
                Inco.technical_account = parametres.RMI_ISC_CtSuggested;
                Inco.cin = parametres.RMI_ISC_CinSuggested;
                Inco.status_transaction = NewStatus;

                _repoContext.Set<amen_transaction_payee>().Update(Inco);
                _repoContext.Set<APH_HDR_HistoriqueDesRapprochements>().Add(IncoherenceHistorique);
                _repoContext.SaveChanges();

            }
            else if (parametres.RMI_Source == "AWB")
            {
                var Inco = _repoContext.Set<attijeri_transaction_payee>().Where(dou => dou.id.Equals(parametres.RMI_ID)).FirstOrDefault();
                var V_Inco = _repoContext.Set<APH_RMI_RapprochementManuelleIncohernce>().Where(dou => dou.RMI_ID.Equals(parametres.RMI_ID) && dou.RMI_Source == "AWB").FirstOrDefault();
                int CheckDouble = _repoContext.Set<attijeri_transaction_payee>()
                    .Where(dou =>
                        (dou.technical_account == parametres.RMI_ISC_CtSuggested) &&
                        (dou.cin == parametres.RMI_ISC_CinSuggested) &&
                        (dou.date_transaction.Value.Year == (Inco.date_transaction.Value.Year)) &&
                        (dou.date_transaction.Value.Month == (Inco.date_transaction.Value.Month)) &&
                        (dou.date_transaction.Value.Day == (Inco.date_transaction.Value.Day))
                    )
                    .ToList().Count;
                string NewStatus = "CORRECTED";
                if (CheckDouble >= 1)
                {
                    NewStatus = "DOUBLE";
                }
                APH_HDR_HistoriqueDesRapprochements IncoherenceHistorique = new APH_HDR_HistoriqueDesRapprochements()
                {
                    HDR_DateTransaction = Inco.date_transaction,
                    HDR_CodePartenaire = "AWB",
                    HDR_CT = Inco.technical_account,
                    HDR_CTRectifie = parametres.RMI_ISC_CtSuggested,
                    HDR_CIN = Inco.cin,
                    HDR_CINRectifie = parametres.RMI_ISC_CinSuggested,
                    HDR_OldStatus = "INCOHERENCE",
                    HDR_NewStatus = NewStatus,
                    HDR_MontantVerse = Inco.amount_monthly_installment,
                    HDR_DateRectification = DateTime.Now,
                    HDR_NomBackOffice = parametres.RMI_UserModified,
                    HDR_Commentaire = parametres.RMI_Commentaire,
                    //HDR_RSM = V_Inco.,
                    HDR_CreatedOn = DateTime.Now,
                    HDR_CreatedBy = parametres.RMI_UserModified,
                    HDR_UpdatedOn = DateTime.Now,
                    HDR_UpdatedBy = parametres.RMI_UserModified
                };
                Inco.technical_account = parametres.RMI_ISC_CtSuggested;
                Inco.cin = parametres.RMI_ISC_CinSuggested;
                Inco.status_transaction = NewStatus;

                _repoContext.Set<attijeri_transaction_payee>().Update(Inco);
                _repoContext.Set<APH_HDR_HistoriqueDesRapprochements>().Add(IncoherenceHistorique);
                _repoContext.SaveChanges();
            }
        }

        public void UpdateIncoherenceManuelle(APH_RMI_RapprochementManuelleIncohernceParameters parametres)
        {
            if (parametres.RMI_Source == "Amen")
            {
                var Inco = _repoContext.Set<amen_transaction_payee>().Where(dou => dou.id.Equals(parametres.RMI_ID)).FirstOrDefault();
                var V_Inco = _repoContext.Set<APH_RMI_RapprochementManuelleIncohernce>().Where(dou => dou.RMI_ID.Equals(parametres.RMI_ID) && dou.RMI_Source == "Amen").FirstOrDefault();
                int CheckDouble = _repoContext.Set<amen_transaction_payee>()
                    .Where(dou =>
                        (dou.technical_account == parametres.RMI_CT) &&
                        (dou.cin == parametres.RMI_CIN) &&
                        (dou.date_transaction.Value.Year == (Inco.date_transaction.Value.Year)) &&
                        (dou.date_transaction.Value.Month == (Inco.date_transaction.Value.Month)) &&
                        (dou.date_transaction.Value.Day == (Inco.date_transaction.Value.Day))
                    )
                    .ToList().Count;
                string NewStatus = "CORRECTED";
                if (CheckDouble >= 1 && parametres.Decision_Manuelle == "CORRECTED")
                {
                    NewStatus = "DOUBLE";
                }
                else
                {
                    NewStatus = String.IsNullOrEmpty(parametres.Decision_Manuelle) ? "CORRECTED" : parametres.Decision_Manuelle;
                }
                string ctToSave = parametres.Decision_Manuelle == "SUSPENDED" ? Inco.technical_account : parametres.RMI_CT;
                string cinToSave = parametres.Decision_Manuelle == "SUSPENDED" ? Inco.cin : parametres.RMI_CIN;

                APH_HDR_HistoriqueDesRapprochements IncoherenceHistorique = new APH_HDR_HistoriqueDesRapprochements()
                {
                    HDR_DateTransaction = Inco.date_transaction,
                    HDR_CodePartenaire = "Amen",
                    HDR_CT = Inco.technical_account,
                    HDR_CTRectifie = ctToSave,
                    HDR_CIN = Inco.cin,
                    HDR_CINRectifie = cinToSave,
                    HDR_OldStatus = "INCOHERENCE",
                    HDR_NewStatus = NewStatus,
                    HDR_MontantVerse = Inco.amount_credit,
                    HDR_DateRectification = DateTime.Now,
                    HDR_NomBackOffice = parametres.RMI_UserModified,
                    HDR_Commentaire = parametres.RMI_Commentaire,
                    //HDR_RSM = V_Inco.,
                    HDR_CreatedOn = DateTime.Now,
                    HDR_CreatedBy = parametres.RMI_UserModified,
                    HDR_UpdatedOn = DateTime.Now,
                    HDR_UpdatedBy = parametres.RMI_UserModified
                };

                Inco.technical_account = string.IsNullOrEmpty(ctToSave) ? Inco.technical_account : ctToSave;
                Inco.cin = string.IsNullOrEmpty(cinToSave) ? Inco.cin : cinToSave;
                Inco.status_transaction = NewStatus;
                //in suspended case check double no_transaction
                if (NewStatus == "SUSPENDED")
                {
                    var checkTransactionDouble = _repoContext.Set<amen_transaction_payee>()
                        .Where(dou => (dou.no_transaction == Inco.no_transaction))
                        .ToList().Count;
                    if (checkTransactionDouble > 1)
                    {
                        Inco.status_transaction = Inco.status_transaction + "-" + DateTime.Now.Millisecond.ToString();
                    }
                }
                _repoContext.Set<amen_transaction_payee>().Update(Inco);
                _repoContext.Set<APH_HDR_HistoriqueDesRapprochements>().Add(IncoherenceHistorique);
                _repoContext.SaveChanges();
            }
            else if (parametres.RMI_Source == "AWB")
            {
                var Inco = _repoContext.Set<attijeri_transaction_payee>().Where(dou => dou.id.Equals(parametres.RMI_ID)).FirstOrDefault();
                var V_Inco = _repoContext.Set<APH_RMI_RapprochementManuelleIncohernce>().Where(dou => dou.RMI_ID.Equals(parametres.RMI_ID) && dou.RMI_Source == "AWB").FirstOrDefault();
                int CheckDouble = _repoContext.Set<attijeri_transaction_payee>()
                    .Where(dou =>
                        (dou.technical_account == parametres.RMI_CT) &&
                        (dou.cin == parametres.RMI_CIN) &&
                        (dou.date_transaction.Value.Year == (Inco.date_transaction.Value.Year)) &&
                        (dou.date_transaction.Value.Month == (Inco.date_transaction.Value.Month)) &&
                        (dou.date_transaction.Value.Day == (Inco.date_transaction.Value.Day))
                    )
                    .ToList().Count;
                string NewStatus = "CORRECTED";
                if (CheckDouble >= 1 && parametres.Decision_Manuelle == "CORRECTED")
                {
                    NewStatus = "DOUBLE";
                }
                else
                {
                    NewStatus = String.IsNullOrEmpty(parametres.Decision_Manuelle) ? "CORRECTED" : parametres.Decision_Manuelle;
                }
                string ctToSave = parametres.Decision_Manuelle == "SUSPENDED" ? Inco.technical_account : parametres.RMI_CT;
                string cinToSave = parametres.Decision_Manuelle == "SUSPENDED" ? Inco.cin : parametres.RMI_CIN;
                APH_HDR_HistoriqueDesRapprochements IncoherenceHistorique = new APH_HDR_HistoriqueDesRapprochements()
                {
                    HDR_DateTransaction = Inco.date_transaction,
                    HDR_CodePartenaire = "AWB",
                    HDR_CT = Inco.technical_account,
                    HDR_CTRectifie = ctToSave,
                    HDR_CIN = Inco.cin,
                    HDR_CINRectifie = cinToSave,
                    HDR_OldStatus = "INCOHERENCE",
                    HDR_NewStatus = NewStatus,
                    HDR_MontantVerse = Inco.amount_monthly_installment,
                    HDR_DateRectification = DateTime.Now,
                    HDR_NomBackOffice = parametres.RMI_UserModified,
                    HDR_Commentaire = parametres.RMI_Commentaire,
                    //HDR_RSM = V_Inco.,
                    HDR_CreatedOn = DateTime.Now,
                    HDR_CreatedBy = parametres.RMI_UserModified,
                    HDR_UpdatedOn = DateTime.Now,
                    HDR_UpdatedBy = parametres.RMI_UserModified
                };

                Inco.technical_account = string.IsNullOrEmpty(ctToSave) ? Inco.technical_account : ctToSave;
                Inco.cin = string.IsNullOrEmpty(cinToSave) ? Inco.cin : cinToSave;
                Inco.status_transaction = NewStatus;
                //in suspended case check double no_transaction
                if (NewStatus == "SUSPENDED")
                {
                    var checkTransactionDouble = _repoContext.Set<attijeri_transaction_payee>()
                        .Where(dou => (dou.no_transaction == Inco.no_transaction))
                        .ToList().Count;
                    if (checkTransactionDouble > 1)
                    {
                        Inco.status_transaction = Inco.status_transaction + "-" + DateTime.Now.Millisecond.ToString();
                    }
                }

                _repoContext.Set<attijeri_transaction_payee>().Update(Inco);
                _repoContext.Set<APH_HDR_HistoriqueDesRapprochements>().Add(IncoherenceHistorique);
                _repoContext.SaveChanges();
            }

        }
        //private void SearchByName(ref IQueryable<APH_V_Monitoring_Partenaire> owners, string param)
        //{
        //    if (!owners.Any() || string.IsNullOrWhiteSpace(param))
        //        return;
        //    owners = owners.Where(o => o.NomBatch.ToLower().Contains(param.Trim().ToLower()));
        //}
    }
}