﻿using Contracts;
using Entities;
using Entities.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Linq.Dynamic.Core;
using System.Linq.Expressions;

namespace Repository
{
    public class APH_VER_VersementEnRouteRepository : RepositoryBase<APH_VER_VersementEnRoute>, IAPH_VER_VersementEnRouteRepository
    {
        private ISortHelper<APH_VER_VersementEnRoute> _sortHelper;
        private RepositoryContext _repositoryContext;
        public APH_VER_VersementEnRouteRepository(RepositoryContext repositoryContext, ISortHelper<APH_VER_VersementEnRoute> sortHelper)
            : base(repositoryContext)
        {
            _sortHelper = sortHelper;
            _repositoryContext = repositoryContext;
        }

        public void CreateVersementEnRoute(APH_VER_VersementEnRoute versementEnRoute)
        {
            Create(versementEnRoute);
        }


        public PagedList<APH_VER_VersementEnRoute> GetVersementEnRoutes(APH_VER_VersementEnRouteParameter Parameters)
        {
            var returnedList = FindByCondition(
               o =>
                   "true" == "true"
           );

            //SearchByName(ref returnedList, Parameters);
            //var filter = new Expression<Func<APH_RDS_RapprochementDesSuspendus, bool>>();

            //var sortedOwners = _sortHelper.ApplySort(owners, ownerParameters.OrderBy);
            var _incoherenceItems =
               _repositoryContext.Set<APH_VER_VersementEnRoute>()
                   .Where(
                       x =>
                           x.VER_Source.ToLower()
                               .Contains(Parameters.VER_Partenaire)
                           || string.IsNullOrEmpty(Parameters.VER_Partenaire)
                    )
                   .Where(
                       x =>
                           x.VER_DateTransaction.Equals(Parameters.VER_DateOperation)
                           || Parameters.VER_DateOperation == null
                    )
                   .AsNoTracking()
                   .AsQueryable()
                   .OrderByDescending(x => x.VER_DateTransaction);
            return PagedList<APH_VER_VersementEnRoute>
                    .ToPagedList(_incoherenceItems, 1, 1000);
                 
        }

        private void SearchByName(ref IQueryable<APH_VER_VersementEnRoute> owners, APH_VER_VersementEnRouteParameter parameters)
        {
            bool ok = false;

            if (!owners.Any() || (parameters.VER_DateOperation != null && parameters.VER_Partenaire != "Tous") && ok == false)
            {
                owners = owners.Where(o => o.VER_DateTransaction.Equals(parameters.VER_DateOperation))
                               .Where(o => o.VER_Source.Equals(parameters.VER_Partenaire.Trim()));
                ok = true;
            }
            if (!owners.Any() || (parameters.VER_DateOperation != null && (parameters.VER_Partenaire == "Tous")) && ok == false)
            {
                owners = owners.Where(o => o.VER_DateTransaction.Equals(parameters.VER_DateOperation));
                ok = true;
            }

            if (!owners.Any() || (parameters.VER_Partenaire != "Tous" && parameters.VER_DateOperation == null) && ok == false)
            {
                owners = owners.Where(o => o.VER_Source.Equals(parameters.VER_Partenaire.Trim()));
                ok = true;
            }

            if (!owners.Any() || (parameters.VER_Partenaire == "Tous" && parameters.VER_DateOperation == null) && ok == false)
            {
                owners = owners.AsQueryable();
                ok = true;
            }

        
        }

        public APH_VER_VersementEnRoute GetVersementEnRoutesById(int IdVer)
        {
            return FindByCondition(sus => sus.VER_ID.Equals(IdVer))
                 .FirstOrDefault();
        }


 
        public void UpdateVersementEnRoute(APH_VER_VersementEnRoute ver)
        {
            Update(ver);
        }

        public APH_VER_VersementEnRoute UpdateVersementEnRouteObject(APH_VER_VersementEnRouteParameter Parameters)
        {
            //var Ver = GetVersementEnRoutesById(Parameters.VER_ID);
            //Ver.VER_Decision= Parameters.VER_Decision;
            //Update(Ver);
            //return Ver;
            return null;
        }

        public void UpdateArray(APH_VER_VersementEnRouteParameter[] tables)
        {
            List<amen_transaction_payee> liste = new List<amen_transaction_payee>();
            for (int i = 0; i < tables.Length; i++)
            {
                var Ver = _repositoryContext.Set<amen_transaction_payee>().Where(dou => dou.id.Equals(tables[i].VER_ID)).FirstOrDefault();
                Ver.status_transaction = tables[i].VER_Decision;
                if (tables[i].VER_Decision == "SUSPENDED")
                {
                    Ver.no_transaction = "VR-"
                        + DateTime.Now.Year.ToString() 
                        + DateTime.Now.Month.ToString() 
                        + DateTime.Now.Day.ToString() 
                        + DateTime.Now.Hour.ToString() 
                        + DateTime.Now.Minute.ToString() 
                        + DateTime.Now.Second.ToString() 
                        + DateTime.Now.Millisecond.ToString() 
                        + Ver.id;
                }
                liste.Add(Ver);
                APH_HDR_HistoriqueDesRapprochements Historique = new APH_HDR_HistoriqueDesRapprochements()
                {
                    HDR_DateTransaction = Ver.date_transaction,
                    HDR_CodePartenaire = "Amen",
                    HDR_CT = Ver.technical_account,
                    HDR_CTRectifie = "",
                    HDR_CIN = Ver.cin,
                    HDR_CINRectifie = "",
                    HDR_OldStatus = "CASH_PAYMENT",
                    HDR_NewStatus = Ver.status_transaction,
                    HDR_MontantVerse = Ver.amount_credit,
                    HDR_DateRectification = DateTime.Now,
                    HDR_NomBackOffice = tables[i].VER_UserModified,
                    HDR_Commentaire = "",
                    //HDR_RSM = V_Inco.,
                    HDR_CreatedOn = DateTime.Now,
                    HDR_CreatedBy = tables[i].VER_UserModified,
                    HDR_UpdatedOn = DateTime.Now,
                    HDR_UpdatedBy = tables[i].VER_UserModified
                };
                _repositoryContext.Set<APH_HDR_HistoriqueDesRapprochements>().Add(Historique);
            }
            _repositoryContext.Set<amen_transaction_payee>().UpdateRange(liste);
            _repositoryContext.SaveChanges();
        }
    }
}