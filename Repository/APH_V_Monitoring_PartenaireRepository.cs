﻿using Contracts;
using Entities;
using Entities.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Linq.Dynamic.Core;
using System.Net.Http;
using System.Threading.Tasks;

namespace Repository
{
    public class APH_V_Monitoring_PartenaireRepository : RepositoryBase<APH_V_Monitoring_Partenaire>, IAPH_V_Monitoring_PartenaireRepository
    {
        private ISortHelper<APH_V_Monitoring_Partenaire> _sortHelper;
        private RepositoryContext _repoContext;
        private static readonly HttpClient client = new HttpClient();
        public APH_V_Monitoring_PartenaireRepository(RepositoryContext repositoryContext, ISortHelper<APH_V_Monitoring_Partenaire> sortHelper)
            : base(repositoryContext)
        {
            _sortHelper = sortHelper;
            _repoContext = repositoryContext;

        }

        public async Task EnvoiAmplitude(string source, string Statuts)
        {
            // PROD
           // var response = await client.GetAsync("http://172.16.91.202:8040/services/TUN/CorrectCannaux/API/corrected?statuts=" + Statuts + "&source=" + source);
            //UAT
            //var response = await client.GetAsync("http://10.110.1.55:8040/services/TUN/CorrectCannaux/API/corrected?statuts=" + Statuts + "&source=" + source);
            //var response = await client.GetAsync("http://localhost:5000/api/monitoring");




        }

        public IList<APH_V_Monitoring_Partenaire> GetMonitoring(APH_V_Monitoring_PartenaireParameters Parameters)
        {
            var returnedList = FindByCondition(
                o =>
                    "true" == "true"
            );


            var _MonitoringListe =
                _repoContext.Set<APH_V_Monitoring_Partenaire>()
                    .Where(
                        x =>
                            x.MOP_Source.ToLower()
                                .Contains(Parameters.MOP_Source)
                            || string.IsNullOrEmpty(Parameters.MOP_Source)
                     )
                    .Where(
                        x =>
                            x.MOP_DateOperation.Equals(Parameters.MOP_DateOperation)
                            || Parameters.MOP_DateOperation == null
                     )
                    .AsNoTracking()
                    .AsQueryable();



            return PagedList<APH_V_Monitoring_Partenaire>
                    .ToPagedList(_MonitoringListe, Parameters.PageNumber, 500);


            //SearchByName(ref returnedList, Parameters.Name);
            //var sortedOwners = _sortHelper.ApplySort(owners, ownerParameters.OrderBy);
            //return PagedList<APH_V_Monitoring_Partenaire>.ToPagedList(returnedList,
            //    Parameters.PageNumber,
            //    Parameters.PageSize);
            //return returnedList.ToList();
        }

        public IList<APH_V_TRR_TransactionRejeter> GetRejet(APH_Extract_Parameters Parameters)
        {

            var TraansactionRejeter =
             _repoContext.Set<APH_V_TRR_TransactionRejeter>()
                 .Where(
                     x =>
                         x.TRR_Partner.ToLower()
                             .Contains(Parameters.CodePartenaire.ToLower())
                         || string.IsNullOrEmpty(Parameters.CodePartenaire)
                  )

                 .Where(
                     x =>
                         x.TRR_DueDate.Value.Date >= Parameters.DateOperation || !Parameters.DateOperation.HasValue
                 )
                 .Where(
                     x =>
                         x.TRR_DueDate.Value.Date <= Parameters.DateOperation_Fin || !Parameters.DateOperation_Fin.HasValue
                 )
                 .AsNoTracking()
                 .AsQueryable();
            var sorteditem = TraansactionRejeter.OrderByDescending(x => x.TRR_DueDate);
            return PagedList<APH_V_TRR_TransactionRejeter>
                    .ToPagedList(sorteditem, 1, 5000);


        }
        public IList<APH_V_TRS_TransactionSuspendus> GetSuspendus(APH_Extract_Parameters Parameters)
        {
            var TraansactionRejeter =
            _repoContext.Set<APH_V_TRS_TransactionSuspendus>()
                .Where(
                    x =>
                        x.TRS_Partenaire.ToLower()
                            .Contains(Parameters.CodePartenaire.ToLower())
                        || string.IsNullOrEmpty(Parameters.CodePartenaire)
                 )

                .Where(
                    x =>
                        x.TRS_DateDiffusion.Value.Date >= Parameters.DateOperation || !Parameters.DateOperation.HasValue
                )
                .Where(
                    x =>
                        x.TRS_DateDiffusion.Value.Date <= Parameters.DateOperation_Fin || !Parameters.DateOperation_Fin.HasValue
                )
                .AsNoTracking()
                .AsQueryable();
            var sorteditem = TraansactionRejeter.OrderByDescending(x => x.TRS_DateDiffusion);
            return PagedList<APH_V_TRS_TransactionSuspendus>
                    .ToPagedList(sorteditem, 1, 5000);
        }

        public IList<APH_V_TRT_TransactionTransferer> GetSuccess(APH_Extract_Parameters Parameters)
        {
            var TraansactionRejeter =
         _repoContext.Set<APH_V_TRT_TransactionTransferer>()
             .Where(
                 x =>
                     x.TRT_Partenaire.ToLower()
                         .Contains(Parameters.CodePartenaire.ToLower())
                     || string.IsNullOrEmpty(Parameters.CodePartenaire)
              )

             .Where(
                 x =>
                     x.TRT_DateInsertion.Value.Date >= Parameters.DateOperation || !Parameters.DateOperation.HasValue
             )
             .Where(
                 x =>
                     x.TRT_DateInsertion.Value.Date <= Parameters.DateOperation_Fin || !Parameters.DateOperation_Fin.HasValue
             )
             .AsNoTracking()
             .AsQueryable();
            var sorteditem = TraansactionRejeter.OrderByDescending(x => x.TRT_DateTransaction);
            return PagedList<APH_V_TRT_TransactionTransferer>
                    .ToPagedList(sorteditem, 1, 5000);
        }

        public IList<APH_V_TRC_TransactionReconciliation> GetReconciliation(APH_Extract_Parameters Parameters)
        {
            var TraansactionRejeter =
   _repoContext.Set<APH_V_TRC_TransactionReconciliation>()
       .Where(
           x =>
               x.TRC_Partenaire.ToLower()
                   .Contains(Parameters.CodePartenaire.ToLower())
               || string.IsNullOrEmpty(Parameters.CodePartenaire)
        )

       .Where(
           x =>
               x.TRC_DateTransaction.Value.Date >= Parameters.DateOperation || !Parameters.DateOperation.HasValue
       )
       .Where(
           x =>
               x.TRC_DateTransaction.Value.Date <= Parameters.DateOperation_Fin || !Parameters.DateOperation_Fin.HasValue
       )
       .AsNoTracking()
       .AsQueryable();
            var sorteditem = TraansactionRejeter.OrderByDescending(x => x.TRC_DateTransaction);
            return PagedList<APH_V_TRC_TransactionReconciliation>
                    .ToPagedList(sorteditem, 1, 5000);
        }

        public IList<APH_V_RER_RegulationRompus> GetRompus(APH_Extract_Parameters Parameters)
        {
            var TraansactionRejeter = 
                _repoContext.Set<APH_V_RER_RegulationRompus>()
                    .Where(
                    x =>
                        x.RER_DateTransaction.Value.Date >= Parameters.DateOperation || !Parameters.DateOperation.HasValue
                    )
                    .Where(
                    x =>
                        x.RER_DateTransaction.Value.Date <= Parameters.DateOperation_Fin || !Parameters.DateOperation_Fin.HasValue
                    )
                    .AsNoTracking()
                    .AsQueryable();
            var sorteditem = TraansactionRejeter.OrderByDescending(x => x.RER_DateTransaction);
            return PagedList<APH_V_RER_RegulationRompus>
                    .ToPagedList(sorteditem, 1, 5000);
        }



        public IList<APH_V_HSC_HistoriqueSuspendusCorriger> GetSuspendusHis(APH_Extract_Parameters Parameters)
        {
            var TraansactionRejeter =
            _repoContext.Set<APH_V_HSC_HistoriqueSuspendusCorriger>()
                .Where(
                    x =>
                        x.HSC_CodePArtenaire.ToLower()
                            .Contains(Parameters.CodePartenaire.ToLower())
                        || string.IsNullOrEmpty(Parameters.CodePartenaire)
                 )
                .Where(
                    x =>
                        (Parameters.DateOperation != null && x.HSC_DateTransaction.Value.Date >= Parameters.DateOperation)
                        &&
                        (Parameters.DateOperation_Fin != null && x.HSC_DateTransaction.Value.Date <= Parameters.DateOperation_Fin)
                )
                //.Where(
                //    x =>
                //        x.EPF_DateEchance.Value.Date <= Parameters.DateOperation_Fin || !Parameters.DateOperation_Fin.HasValue
                //)
                .AsNoTracking()
                .AsQueryable();
            var sorteditem = TraansactionRejeter.OrderByDescending(x => x.HSC_DateTransaction);
            return PagedList<APH_V_HSC_HistoriqueSuspendusCorriger>
                    .ToPagedList(sorteditem, 1, 5000);

        }
        public IList<APH_V_EPF_EcheancePartenaireFuture> GetEpf(APH_Extract_Parameters Parameters)
        {
            var TraansactionRejeter =
                _repoContext.Set<APH_V_EPF_EcheancePartenaireFuture>()
                    .Where(
                        x =>
                            x.EPF_Partenaire.ToLower()
                                .Contains(Parameters.CodePartenaire.ToLower())
                            || string.IsNullOrEmpty(Parameters.CodePartenaire)
                     )

                    .Where(
                        x =>
                            (Parameters.DateOperation != null && x.EPF_DateEchance.Value.Date >= Parameters.DateOperation)
                            &&
                            (Parameters.DateOperation_Fin != null && x.EPF_DateEchance.Value.Date <= Parameters.DateOperation_Fin)
                    )
                    //.Where(
                    //    x =>
                    //        x.EPF_DateEchance.Value.Date <= Parameters.DateOperation_Fin || !Parameters.DateOperation_Fin.HasValue
                    //)
                    .AsNoTracking()
                    .AsQueryable();
            var sorteditem = TraansactionRejeter.OrderByDescending(x => x.EPF_DateEchance);
            return PagedList<APH_V_EPF_EcheancePartenaireFuture>
                    .ToPagedList(sorteditem, 1, 5000);
        }
    }
}
