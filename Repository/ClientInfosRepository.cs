﻿using System.Linq;
using Contracts;
using Entities;
using Entities.Models;
using Microsoft.EntityFrameworkCore;

namespace Repository
{
    public class ClientInfosRepository : RepositoryBase<ClientInfos>, IClientInfosRepository
    {
        private readonly RepositoryContext _repositoryContext;

        public ClientInfosRepository(RepositoryContext repositoryContext) : base(
            repositoryContext)
        {
            _repositoryContext = repositoryContext;
        }

        public PagedList<ClientInfos> GetClientInfos(ClientInfosParameters parameters)
        {
            var clientInfos = _repositoryContext.Set<ClientInfos>()
                .Where(_ => _.IdClient == parameters.IdClient)
                .AsNoTracking()
                .AsQueryable();
            var sortedItem = clientInfos.OrderBy(_ => _.DateCreated);
            return PagedList<ClientInfos>
                .ToPagedList(sortedItem,
                    parameters.PageNumber,
                    /*parameters.PageSize*/500);
        }
    }
}